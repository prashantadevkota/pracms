<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Image_model extends BF_Model
{
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

	}//end __construct()

	/**
	 * Crops the image according to the parameters passed
	 * $data['imgDim'] => x, y, w, h
	 * $data['src'] = Source image file
	 * $data['dest'] = Destination image file
	 */
	public function cropImage( $data )
	{
		$retval = array( 'status' =>0, 'message' => 'Unknown error' );
		$imgDim = 	$data['imgDim'];
		$targ_w = 	$imgDim['w']; //150;
		$targ_h = 	$imgDim['h']; //150;
		$src = 		$data['src'];
		$dest = 	$data['dest'];
		$jpeg_quality = 90;

		// *******************************************
		if ( file_exists($src) )	// If the file exists
		{
			$imageData = getimagesize($src);
			// $imageData[2] will contain the value of one of the constants
			$mimeType = 	image_type_to_mime_type($imageData[2]);
			$extension = 	image_type_to_extension($imageData[2]);
			
			if( $mimeType == 'image/jpeg' || $extension == '.jpeg' 
				|| $mimeType == 'image/jpg' || $extension == '.jpg' )
			{
				$img_r = imagecreatefromjpeg($src);
			}
			elseif( $mimeType == 'image/png' || $extension == '.png' )
			{
				$img_r = @imagecreatefrompng($src);
			}
			elseif( $mimeType == 'image/gif' || $extension == '.gif' )
			{
				$img_r = @imagecreatefromgif($src);
			}
			else	// Just Brute Force method
			{
				$img_r = @imagecreatefromjpeg($src);
				if ( !$img_r )
					$img_r = @imagecreatefrompng($src);
				if ( !$img_r )
					$img_r = @imagecreatefromgif($src);
			}
			// *******************************************

			if ( $img_r )	// If the image file exists & the image format is supported
			{
				//die("Image format not supported");
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled( $dst_r, $img_r, 0, 0, $imgDim['x'], $imgDim['y'],
				$targ_w, $targ_h, $imgDim['w'], $imgDim['h'] );

				$res = imagejpeg($dst_r, $dest, $jpeg_quality);
				if ( $res )		// If image crop was successful
				{
					$retval = array( 'status' =>1, 'message' => 'Successfully cropped your image' );
				}
				else	// If image crop wasn't successful
				{
					$retval['message'] = error_get_last();
				}
			}
		}
		else	// If the file doesn't exist
		{
			$retval['messsage'] = 'File does not exist';
		}
		return $retval;
	}

	//
	/**
	 * Saves 3 sizes of the image, i.e. small, medium & large. Also copies original file is the appropriate flag is set.
	 *
	 * @param unknown $imgName     The name that you want to give the saved image (excluding extension). Will have '.jpg' ext always.
	 * @param unknown $id     Entity ID
	 * @param string $entity  Entity either 'user', 'church', 'serve_opportunity'
	 * @param string $srcImg   The source image you want to save sized images from.
	 * @param number $flgCopyOrg   Flag to copy the original file after saving sized images.
	 * @return assoc_array
	 */
	public function saveSizedImages( $imgName, $id, $entity='user', $srcImg='', $flgCopyOrg=0 )
	{
		$retval = 			array('status'=>1, 'message' => 'Success');
		$entity = 			strtolower($entity);

		if( $entity == 'user')
			$baseImgPath = getFullUserDirPathFromUserId($id) . '/profile_pics';
		elseif( $entity == 'church' )
			$baseImgPath = getFullChurchDirPathFromChurchId($id) . '/profile_pics';
		elseif( $entity == 'serve_opportunity' )
			$baseImgPath = getFullServeDirPathFromServeId($id) . '/pics';
		elseif( $entity == 'small_group' )
			$baseImgPath = getFullSmallGroupDirPathFromSmallGroupId($id) . '/profile_pics';
		else
			$baseImgPath = '';
// echo "baseImgPath=$baseImgPath, srcImg=$srcImg"; die;
		if( $baseImgPath == '' )	// Not recognized entity
		{
			$retval = 			array('status'=>0, 'message' => 'Entity not recognized');
		}
		else		// A recognized entity
		{
		    if( $srcImg == '' )
		    {
			     $srcImg = 			"$baseImgPath/$imgName.jpg";
		    }
			$sizes = 			array('large', 'medium', 'small');
			$imgDims = 			array(
										'large' => 		array('h'=>200,'w'=>200)
										,'medium' => 	array('h'=>100,'w'=>100)
										,'small' => 	array('h'=>50,'w'=>50)
								);

			$flgError = 0;
			foreach( $sizes as $size )
			{
				$destImg = "$baseImgPath/{$imgName}_{$size}.jpg";
				$res = resize_image($srcImg, $destImg, $imgDims[$size]['h'], $imgDims[$size]['w']);
				if ( !$res['status'] )	// If an error occurred while resizing the image
				{
					$flgError = 	1;
					$retval = 		array( 'status'=>0, 'message' => $res['message'] );
					break;
				}
			}

			if( !$flgError && $flgCopyOrg )  // If no error occurred & we have to copy the original
			{
			    $destImg = "{$baseImgPath}/{$imgName}.jpg";
                if( !copy($srcImg, $destImg) )  // If the copying of the original failed
                {
                    $retval = 		array( 'status'=>0, 'message' => error_get_last() );
                }
			}
		}
		return $retval;
	}

	/** USER. Only works with POST data. The HTML file_inputs should have the same name as the profile_picture.
	 *  Ex: profile, profile1, profile2, profile3, profile4, logo etc.
	 */
	public function uploadProfilePicsByEntityTypeAndUid( $entity, $id, $params=array() )
	{
		$retval = 	array('status' => 1, 'message' => 'Your images were uploaded successfully');
		$entity = 	strtolower($entity);
		if ( $entity == 'user' )
			$base_path = 		getFullUserDirPathFromUserId($id) . '/profile_pics/';
		elseif( $entity == 'church' )
			$base_path = 		getFullChurchDirPathFromChurchId($id) . '/profile_pics/';
		elseif($entity == 'serve_opportunity')
			$base_path = 		getFullServeDirPathFromServeId($id) . '/pics/';
		elseif($entity == 'small_group')
			$base_path = 		getFullSmallGroupDirPathFromSmallGroupId($id) . '/profile_pics/';
		else	// Entity type not recognized
			return array('status' => 0, 'message' => 'Entity type not recognized');
		//---------
		$max_size = 		isset($params['max_size'])  
								? $params['max_size']
								: '2097152';		// 1048576 bytes = 1 MB, 2097152 = 2 MB
		$max_width = 		isset($params['max_width'])
								? $params['max_width']
								: '1200';
 	    $max_height = 		isset($params['max_height'])
 	    						? $params['max_height']
								: '1200';
		//---------
 	    $imageSizes = 		array(
				'large' => array(
						'width' => 200,
						'height' => 200
				),
				'medium' => array(
						'width' => 100,
						'height' => 100
				),
				'small' => array(
						'width' => 50,
						'height' => 50
				)
		);
		$allowed_types = 	'gif|jpg|png|jpeg';
		// ---------------------------------------------

		$config ['upload_path'] = 	$base_path;
		$config ['allowed_types'] = $allowed_types;
		$config ['max_size'] = 		$max_size;
		$config ['max_width'] = 	$max_width;
		$config ['max_height'] = 	$max_height;
		$config ['overwrite'] = 	TRUE;
		//
		$this->load->library ( 'upload', $config );
		$flgError =					0;
		$flgNotBlank =				0;
		foreach($_FILES as $key => $image )
		{
			if( $image['name'] != '' )		// Only if the file was selected to be uploaded
			{
				$flgNotBlank = 1;

				$config ['file_name'] = 	"$key.jpg";
				$this->upload->initialize ( $config );

				if ( $this->upload->do_upload ($key) )		// If uploading was SUCCESSFUL
				{
					$srcImg = 		$config ['upload_path'] . $config ['file_name'];
					$destImgL = 	$config ['upload_path'] . $key . '_large.jpg';
					$destImgM = 	$config ['upload_path'] . $key . '_medium.jpg';
					$destImgS = 	$config ['upload_path'] . $key . '_small.jpg';
					$destImg =		$config ['upload_path'] . $key . '.jpg';

					$resriL = 		resize_image( $srcImg, $destImgL, $imageSizes['large']['height'], $imageSizes['large']['width'] );
					$resriM = 		resize_image( $srcImg, $destImgM, $imageSizes['medium']['height'], $imageSizes['medium']['width'] );
					$resriS = 		resize_image( $srcImg, $destImgS, $imageSizes['small']['height'], $imageSizes['small']['width'] );
					if ( !$resriL['status'] || !$resriM['status'] || !$resriS['status'] )	// An error occurred while resizing
					{
						$flgError = 1;
						$retval = 	array( 'status'=>0,'message'=> 'Error while resizing image' );
					}
				}
				else		// If an error occurred while uploading
				{
					$flgError = 1;
					$retval = 	array( 'status'=>0,'message'=> $this->upload->display_errors('<p>','</p>') );
				}
			}
		}
		return $retval;
	}

	/**
	 * Uploads pics to SYS_TEMP_DIR.
	 * @return array('status','message',
	 *     'data' => Assoc_array that contains the full path of files that were uploaded. Index will be the HMTL input name.
	 * Ex:
	 *     'data' => 'serve1' => full_path
	 */
	public function uploadPicsToTempDir( $params=array() )
	{
	    $retval = 	array('status' => 1, 'message' => 'Your images were uploaded successfully', 'data' => array() );

		$max_size = 		isset($params['max_size'])  
								? $params['max_size']
								: '2097152';		// 1048576 bytes = 1 MB, 2097152 = 2 MB
		$max_width = 		isset($params['max_width'])
								? $params['max_width']
								: '1200';
 	    $max_height = 		isset($params['max_height'])
 	    						? $params['max_height']
								: '1200';
	    $allowed_types = 	'gif|jpg|png|jpeg';
	    // ---------------------------------------------

        $config =                   array();
        $config ['upload_path'] =   SYS_TEMP_DIR;
	    $config ['allowed_types'] = $allowed_types;
		$config ['max_size'] = 		$max_size;
		$config ['max_width'] = 	$max_width;
		$config ['max_height'] = 	$max_height;
	    $config ['overwrite'] = 	TRUE;
	    //
	    $this->load->library ( 'upload', $config );
        $filenames =                array();
        $flgError =                 0;
	    foreach($_FILES as $key => $image )
	    {
//echo "<br/>key=$key, $image=>$image";
	        if( $image['name'] != '' )		// Only if the file was selected to be uploaded
	        {
                $ext = pathinfo($image['name'], PATHINFO_EXTENSION);    // Get the file extension
    	        $config ['file_name'] = md5(microtime()) . ".{$ext}";
    			$this->upload->initialize ( $config );

			    $res = $this->upload->do_upload ($key);
                if( $res )  // If uploading was successful
                {
                    $tmpData =          $this->upload->data();
                    $filenames[$key] =  $tmpData['full_path'];
                }
	            else		// If an error occurred while uploading
	            {
	                $flgError = 1;
	                $retval['status'] = 0;
	                $retval['message'] = $this->upload->display_errors('<p>','</p>');
	                break;     // Exit the loop
	            }
	        }
	    }

	    if( !$flgError )   // If all the files were uploaded successfully
	    {
	        $retval['data'] = $filenames;
	    }

	    return $retval;
	}
	
	/**
	 * 
	 * @param string $entity
	 * @param integer $entId
	 * @param string/array $imgName. Ex: 'pic1' OR array('pic1','pic2','pic3');	
	 * @param string $size.	'all' || 'large' || 'medium' || 'small' || 'original'
	 * @return assoc_array
	 */
	public function deletePicByEntityId( $entity, $entId, $imgNames, $size='all' )
	{
		$retval = 		array( 'status' => 1, 'message' => 'Image successfully deleted' );
		//
		$entity = 		strtolower($entity);
		$size = 		strtolower($size);
		$imgNames = 	is_array($imgNames) ? $imgNames : array($imgNames);
		$sizeList =		$size == 'all' ? array( 'large', 'medium', 'small', 'original' ) : array($size);
		//
		if( $entity == 'user')
			$baseImgPath = getFullUserDirPathFromUserId($entId) . '/profile_pics';
		elseif( $entity == 'church' )
			$baseImgPath = getFullChurchDirPathFromChurchId($entId) . '/profile_pics';
		elseif( $entity == 'serve_opportunity' )
			$baseImgPath = getFullServeDirPathFromServeId($entId) . '/pics';
		elseif( $entity == 'small_group' )
			$baseImgPath = getFullSmallGroupDirPathFromSmallGroupId($entId) . '/profile_pics';
		else
			$baseImgPath = '';
// echo "baseImgPath=$baseImgPath, srcImg=$srcImg"; die;
		if( $baseImgPath == '' )	// Not recognized entity
		{
			$retval = 			array('status'=>0, 'message' => 'Entity not recognized');
		}
		else		// A recognized entity
		{
			$flgError = 	0;		// Error flag
	
			foreach( $imgNames as $img )
			{
				foreach( $sizeList as $size1 )
				{
					if( $size1 == 'original' )
						$fileName = 	"{$baseImgPath}/{$img}.jpg";
					else
						$fileName = 	"{$baseImgPath}/{$img}_{$size1}.jpg";
						
					if (  file_exists($fileName) )	// If file exists
					{
						if( !unlink( $fileName ) )	// If file deletion failed for some reason
						{
							$flgError = 1;		// Set the error flag
							$retval = array( 'status' => 0, 'message' => "Error while deleting {$fileName}" );
						}
					}
					else	// The file doesn't exist
					{
						$flgError = 1;		// Set the error flag
						$retval = array( 'status' => 0, 'message' => "{$fileName} doesn't exist!" );
					}
				}
			}
		}
		return $retval;
	}
}