<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class User_model extends CI_Model
{
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

	}//end __construct()

	public function tryLogin( $userName, $password, $loginType='admin' )
	{
		$retval = 	array( 'status' => 0, 'message' => 'Username and password do not match', 'data'=>array() );
		$roleId = 	$loginType == 'user' ? 2 : 1;
		
		// First determine if the username(email) exists
		$sql = 		"SELECT 
						u.user_id as uid, u.salt, u.password, u.role_id, u.status
					FROM
						tbl_user as u
					WHERE
						email COLLATE UTF8_GENERAL_CI='$userName'
						AND role_id=$roleId
					";
		$query = 	$this->db->query($sql, array($userName) );
		$res = 		$query->result();
		if ( $res )	// If the user_name exists
		{
			if ( $res[0]->status == 1 )		// If the user is active
			{
				$salt = 		$res[0]->salt;
				$dbPassword = 	$res[0]->password;
				$uid = 			$res[0]->uid;
				$roleId = 		$res[0]->role_id;
				if ( do_hash($salt . $password) == $dbPassword )		// If the password matches
				{
					storeUserDetailsInSession( array( 'uid' => $uid ) );
					$retval = array( 'status'=>1, 'message'=>'Success', 'data' => array('uid'=>$uid, 'roleId' => $roleId ) );
				}
			}
		}
		else
		{
			echo mysql_error();
		}
		return $retval;
	}
}