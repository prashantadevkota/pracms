<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Product_model extends CI_Model
{
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

	}//end __construct()

	public function createProduct()
	{
		$retval = 		array('status'=>1, 'message'=>'success');
		
		$title = 		$this->input->post('txtTitle');
		$price = 		$this->input->post('txtPrice');
		$desc = 		$this->input->post('txaDescription');
		
		$data = 		array( 'title' => $title, 'price' => $price, 'description' => $desc, 'create_time' => TIME_GMT );
		$res = 			$this->db->insert ( 'tbl_product', $data );
		if ( $res )	// If the activity was successfully inserted
		{
			$prodId = 	$this->db->insert_id();
			if( !$this->createProductDirectoryFromProdId($prodId) )
			{
				$retval = 	array( 'status'=>0, 'message'=> 'Could not create product directory. Please check permissions.' );
				return $retval;
			}
			
			// Now handle the image (if any)
			$res2 = 	$this->uploadImageForProductUsingProdId( $prodId );
			if( !$res2['status'] )	// If image uploading failed
			{
				$retval = 	array( 'status'=>0, 'message'=> $res2['message'] );
			}
		}
		else
		{
			$retval = 	array( 'status'=>0, 'message'=>'Product creation failed');
		}
		
		return $retval;
	}
	
	public function editProduct( $prodId )
	{
		$retval = 		array('status'=>1, 'message'=>'success');
		
		$title = 		$this->input->post('txtTitle');
		$price = 		$this->input->post('txtPrice');
		$desc = 		$this->input->post('txaDescription');
		
		$sql = 			"UPDATE
							tbl_product
						SET
							title=?,
							price=?,
							description=?
						WHERE
							prod_id=?
						";
		$data = 		array( $title, $price, $desc, $prodId );
		$res = 			$this->db->query( $sql, $data );
		if ( $res )	// If the product was successfully updated
		{
			// Now handle the image (if any)
			$res2 = 	$this->uploadImageForProductUsingProdId( $prodId );
			if( !$res2['status'] )	// If image uploading failed
			{
				$retval = 	array( 'status'=>0, 'message'=> $res2['message'] );
			}
		}
		else
		{
			$retval = 	array( 'status'=>0, 'message'=>'Product creation failed');
		}
		
		return $retval;
	}
	
	public function createProductDirectoryFromProdId( $prodId )
	{
		$dir_permissions = 		0777;  // '0777' is full access to everyone
		$base_path = 			getProductDirPathFromProdId($prodId);
		
		// Now create both directories (recursive subdirectory creation as well)
		if ( !mkdir( $base_path, $dir_permissions, TRUE ) )	// If an error occurred
			return false;
		else
			return true;
	}
	
	public function uploadImageForProductUsingProdId( $prodId )
	{
		$retval = 	array('status' => 1, 'message' => 'Your images were uploaded successfully');
		
		$base_path = 		getProductDirPathFromProdId($prodId);
		$allowed_types = 	'gif|jpg|png|jpeg';
		// ---------------------------------------------
	
		$config ['upload_path'] = 	$base_path;
		$config ['allowed_types'] = $allowed_types;
// 		$config ['max_size'] = 		$max_size;
// 		$config ['max_width'] = 	$max_width;
// 		$config ['max_height'] = 	$max_height;
		$config ['overwrite'] = 	TRUE;
		//
		$this->load->library ( 'upload', $config );
		$flgError =					0;
		$flgNotBlank =				0;
		foreach($_FILES as $key => $image )
		{
			if( $image['name'] != '' )		// Only if the file was selected to be uploaded
			{
				$flgNotBlank = 1;
	
				$config ['file_name'] = 	"img1.jpg";
				$this->upload->initialize ( $config );
	
				if ( !$this->upload->do_upload ($key) )		// If uploading failed
				{
					$flgError = 1;
					$retval = 	array( 'status'=>0,'message'=> $this->upload->display_errors('<p>','</p>') );
				}
			}
			if( $flgError )
				break;
		}
		return $retval;
	}

	public function getList( $sortBy, $listAll=false )
	{
		if( $sortBy == 'alpha' )
		{
			$orderField = 	'title';
			$ascDesc = 		'ASC';
		}
		else	// It's sorting by time
		{
			$orderField = 	'create_time';
			$ascDesc = 		'DESC';
		}
		$limit = 	25;
		$extraCond = 	$listAll == false ? ' WHERE status=1 ' : '';
		
		$sql = 		"SELECT 
						prod_id, 
						title, 
						price, 
						description, 
						create_time, 
						status 
					FROM 
						tbl_product
					$extraCond
					ORDER BY
						$orderField $ascDesc 
					limit $limit
					";
		$res = 		$this->db->query( $sql );
		return $res->result_array();
	}
	
	public function getProductDetails( $prodId=0 )
	{
		$sql = 		"SELECT 
						prod_id, 
						title, 
						price, 
						description, 
						create_time, 
						status 
					FROM 
						tbl_product
					WHERE
						prod_id=?
					limit 1
					";
		$res = 		$this->db->query( $sql, $prodId );
		$res = 		$res->result_array();
		return $res[0];
	}
	
	public function disableProduct( $prodId=0 )
	{
		$sql = 		"UPDATE
						tbl_product
					SET
						status=0
					WHERE
						prod_id=?
					";
		$res = 		$this->db->query( $sql, array($prodId) );
		if( $res )	// If it was successful
		{
			return array( 'status'=>1, 'message'=>'Successfully disabled the product.');
		}
		else	// Some error occurred
		{
			return array( 'status'=>0, 'message'=>'Sorry, could not disable the product.');
		}
	}
	
	public function enableProduct( $prodId=0 )
	{
		$sql = 		"UPDATE
						tbl_product
					SET
						status=1
					WHERE
						prod_id=?
					";
		$res = 		$this->db->query( $sql, array($prodId) );
		if( $res )	// If it was successful
		{
			return array( 'status'=>1, 'message'=>'Successfully enabled the product.');
		}
		else	// Some error occurred
		{
			return array( 'status'=>0, 'message'=>'Sorry, could not enable the product.');
		}
	}
	
}