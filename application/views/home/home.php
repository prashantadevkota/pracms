<?php $this->load->view('common/header'); ?>

<!--main body starts-->
<section class="main-content">
    <div class="wrapper">
        <div class="clear"></div>
        
        <section class="page-content">
        	<?php if( !isAdminLoggedIn() ): ?>
		    <div>
		    	<form name="frmLogin" id="frmLogin" method="post" action="/user/ajax_trylogin">
					Username <input type="text" name="txtUserName" id="txtUserName" />
					Password <input type="password" name="txtPassword" id="txtPassword" />
					<input type="button" value="Login >>" onClick="javascript:tryLogin();" />
		    	</form>
		    </div>
		    <?php else: ?>
				<input type="button" value="Admin" onClick="javascript:window.location='/admin';" />
		    	<input type="button" value="Logout" onClick="javascript:logout();" />
        	<?php endif; ?>
        	
        	<?php
        		if( $sortBy == 'alpha' )	// Alphabetical sort
        		{
        			echo "<a href='?sortby=time'>Sort by Latest</a>";
        			echo " Sort Alphabetically";
        		}
        		else	// Sort by time
        		{
        			echo "Sort by Latest";
        			echo " <a href='?sortby=alpha'>Sort Alphabetically</a>";
        		} 
        	?>
        </section>
        
    <?php 
    $isAdmin = isAdminLoggedIn();
    foreach($list as $row): ?>
	    <div style="float: left; width:300px;" id="prod_<?php echo $row['prod_id'];?>">
	    	<img src="<?php echo getProductImageFromProdId($row['prod_id']), '?', RAND_QSTR; ?>" style="width:200px;" /> <br/>
		    Title: <?php echoCleanedData( $row['title'] ); ?> <br/>
		    Price: $<?php echoCleanedData( $row['price'] ); ?> <br/>
		    Description: <?php echoCleanedData( $row['description'] ); ?> <br/>
		    Added Date: <?php echo $row['create_time']; ?> <br/>
		    
		    <div id="enbdsb_<?php echo $row['prod_id'];?>">
		    <?php 
		    	if( $isAdmin )
		    	{
			    	echo $row['status'] 
			    			? " <a href='javascript:disable({$row['prod_id']});'>Disable</a>"
			    			: " <a href='javascript:enable({$row['prod_id']});'>Enable</a>"
	    			;
	    			
	    			echo " >> <a href='/admin/editproduct/{$row['prod_id']}'>Edit Product</a>";
		    	}
		    ?> 
		    </div>	<br/>
	    </div>
	<?php endforeach; ?>
        
    </div>
</section>

<div class="clear"></div>
<!--main body ends-->
<?php 
	$password = 	'123456';
	$salt = 		'prashanta';
	$pwdHash = 		md5($salt . $password);
// 	echo "<br/>passwordHash=$pwdHash";
?>

<?php $this->load->view('common/footer'); ?>