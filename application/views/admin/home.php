<?php $this->load->view('common/header'); ?>

<!--main body starts-->
<section class="main-content">
    <div class="wrapper">
        <div class="clear"></div>
        
        <section class="page-content">
		    <div>
				<input type="button" value="Home" onClick="javascript:window.location='/';" />
				<input type="button" value="Logout" onClick="javascript:logout();" />
		    </div>
		    <hr/>
		    
        	<p class="pnotfound"> <a href='/admin/newproduct'>Post a new product</a></p>
        </section>
    </div>
</section>

<div class="clear"></div>
<!--main body ends-->

<?php $this->load->view('common/footer'); ?>