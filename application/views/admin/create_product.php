<?php $this->load->view('common/header'); ?>

<!--main body starts-->
<section class="main-content">
    <div class="wrapper">
        <div class="clear"></div>
        
        <section class="page-content">
		    <div>
				<input type="button" value="Logout >>" onClick="javascript:logout();" />
		    </div>
		    <hr/>
		    
		    <?php echo getErrorMessage(); ?> 
        	
        	<p> <h1>Post a new Product</h1> </p>
        	
        	<form name="frmCreateProduct" id="frmCreateProduct" method="post" enctype="multipart/form-data" accept-charset="utf-8">
	        	<p class="product-item">
	        		Title <input type="text" name="txtTitle" id="txtTitle" value="<?php echoCleanedData($product['title']); ?>" /> <br/>
	        		
	        		<img src="<?php echo getProductImageFromProdId($product['prod_id']), '?', RAND_QSTR; ?>" style="width:200px;" /> <br/>
	        		Image <input type="file" name="filImage" id="filImage" /> <br/> <br/>
	        		
	        		Price <input type="text" name="txtPrice" id="txtPrice" value="<?php echoCleanedData($product['price']); ?>" /> <br/>
	        		
	        		Description <textarea rows="4" cols="20" name="txaDescription" id="txaDescription"><?php echoCleanedData($product['description']); ?></textarea> <br/>
	        		
	        		<input type="submit" name="btnSubmit" id="btnSubmit" value="submit" />
	        		<input type="button" id="btnCancel" value="Cancel" onClick="javascript:window.location='/admin/';" />
	        	</p>
        	</form>
        	
        </section>
    </div>
</section>

<div class="clear"></div>
<!--main body ends-->

<?php $this->load->view('common/footer'); ?>