</div> <!-- Closing of wrapper div opened in header -->

<p>&nbsp;</p>
<footer class="wrapper">
	<nav class="footer-nav">
		<span class="fl">&copy; 2014 PraCMS |</span>
		<ul>
			<li><a href="javascript:underConstruction();">Contact Us</a>|</li>
			<li><a href="javascript:underConstruction();">About</a>|</li>
			<li><a href="javascript:underConstruction();">Advertise</a>|</li>
			<li><a href="javascript:underConstruction();">User Agreement</a>|</li>
			<li><a href="javascript:underConstruction();">Privacy Policy</a></li>
		</ul>
	</nav>
	
	<div class="social-icon">
	 	<a href="http://www.facebook.com/pracms" target="_blank" class="fb"></a>
	    <a href="https://twitter.com/@pracms" target="_blank" class="twitter"></a>
	</div>
</footer>