<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="PraCMS is the interview question prepared by Prashanta Prasad Devkota. Mobile: 0422941227, Email: prashantadevkota@hotmail.com.">
<meta name="keywords" content="CMS, Interview, Question">
<title><?php echoCleanedData( PAGE_TITLE_PREFIX . $pageTitle, 'PraCMS'); ?></title>

<script src="<?php echo BASE_URL;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>public/js/jquery.validate.js"></script>

<!-- This JavaScript Code should be after the JS libraries but BEFORE base.js -->
<script type="text/javascript">
	         document.createElement('header');
	         document.createElement('hgroup');
	         document.createElement('nav');
	         document.createElement('menu');
	         document.createElement('section');
	         document.createElement('article');
	         document.createElement('aside');
	         document.createElement('footer');
	 <?php 
	 		echoJsConstants();
    ?>
</script>
<!-- END_OF This JavaScript Code should be after the JS libraries but BEFORE base.js -->

<script type="text/javascript" src="<?php echo BASE_URL;?>public/js/base.js"></script>

<!--  Related with Colorbox -->
<script type="text/javascript" src="<?php echo BASE_URL;?>public/js/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL;?>public/js/colorbox/colorbox.css" media="all"/>
<!--  END_OF Related with Colorbox -->

<!--  Related with Design -->
<link rel="stylesheet"
	href="<?php echo BASE_URL;?>public/css/styles.css" type="text/css"
	media="screen" />
<link rel="stylesheet"
	href="<?php echo BASE_URL;?>public/css/bootstrap.css" type="text/css"
	media="screen" />
<link rel="stylesheet"
	href="<?php echo BASE_URL;?>public/css/bootstrap-responsive.css"
	type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo BASE_URL;?>public/css/media.css"
	type="text/css" media="screen" />
	
<link rel="icon" 
	href="<?php echo BASE_URL;?>public/images/favicon.jpg" type="image/jpeg"/>
<link rel="shortcut icon" 
	href="<?php echo BASE_URL;?>public/images/favicon.jpg" type="image/jpeg"/>
	
<script src="<?php echo BASE_URL;?>public/js/config.js"></script>
<script src="<?php echo BASE_URL;?>public/js/skel.min.js"></script>
<script src="<?php echo BASE_URL;?>public/js/skel-ui.min.js"></script>


<!--[if IE]>
		          <link href="<?php echo BASE_URL;?>public/css/all-ie-only.css" rel="stylesheet" type="text/css" />
		    <![endif]-->
<!--[if IE 7]>
		      <link href="<?php echo BASE_URL;?>public/css/ie7.css" rel="stylesheet" type="text/css" />
		    <![endif]-->

<!--[if IE 8]>
		      <link href="<?php echo BASE_URL;?>public/css/ie8.css" rel="stylesheet" type="text/css" />
		    <![endif]-->

<!--[if IE 9]>
		      <link href="<?php echo BASE_URL;?>public/css/ie9.css" rel="stylesheet" type="text/css" />
		    <![endif]-->
<!-- slider -->
<!--  END_OF Related with Design -->
</head>

<body>
	<div id="wrapper"> <!-- close it in footer -->
		<!--header starts-->

		<div class="clear"></div>
<!--  Fix for the IE placeholder bug -->
<script>
// A jQuery based placeholder polyfill
$(document).ready(function(){
  function add() {
    if($(this).val() === ''){
      $(this).val($(this).attr('placeholder')).addClass('placeholder');
    }
  }

  function remove() {
    if($(this).val() === $(this).attr('placeholder')){
      $(this).val('').removeClass('placeholder');
    }
  }

  // Create a dummy element for feature detection
  if (!('placeholder' in $('<input>')[0])) {

    // Select the elements that have a placeholder attribute
    $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

    // Remove the placeholder text before the form is submitted
    $('form').submit(function(){
      $(this).find('input[placeholder], textarea[placeholder]').each(remove);
    });
  }
});
</script>		
<!--  END_OF Fix for the IE placeholder bug -->

<div class="wrapper">
	<header class="header-area">
		<div class="wrapper">
	        <div class="logo">
	        	<a href="<?php echo BASE_URL; ?>">
	        		<img src="<?php echo BASE_URL; ?>public/images/logo.png"  alt=""/>
	        	</a>
	        </div>
	        
	        <section style="padding-left: 60px;">PraCMS - The CMS you want!</section>
	    </div>
	</header>
</div>
<div class="clear"></div>

<!--header ends-->		