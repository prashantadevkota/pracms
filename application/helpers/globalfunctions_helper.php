<?php
// *********************************
// Global Variables & Constants
// *********************************
define('BASE_URL', 			base_url() );
define('TIME_GMT', 			getGmtTime() );
define( 'OS', 				(substr(php_uname(), 0, 7) == "Windows") ? 'Windows' : 'Linux');
define( 'NEWLINE', 			( OS == 'Windows') ? "\r\n" : "\n" );

// The ending / is for linux systems where the temp directory might not have a slash at the end
define( 'SYS_TEMP_DIR', 	str_replace( '\\', '/', sys_get_temp_dir() ) . '/' );
define( 'RAND_QSTR', 		'?'. substr( md5(rand()),0,4) );
define( 'HACKER_MESSAGE', 	'Your IP is being logged. Please do not tinker with the system. We will report all details to your ISP, if necessary.' );
define('PAGE_TITLE_PREFIX', 'PraCMS::');

// Define our own BASEPATH as MY_BASEPATH
$tmpIdx = strrpos(BASEPATH, SYSDIR );	// Find the position of the system folder in the BASEPATH
if ( $tmpIdx === false )	// If we couldn't located the SYSDIR inside BASEPATH (highly unlikely)
	define('MY_BASEPATH', BASEPATH );	// Just use the BASEPATH of CodeIgniter
else	// We did find the location of the SYSDIR inside BASEPATH
	define('MY_BASEPATH', substr( BASEPATH, 0, $tmpIdx) );	// Define our MY_BASEPATH to point to the root folder
// ---------------

$GLOBALS['CI_INSTANCE'] = get_instance();
// *********************************
// Global Functions
// *********************************

function getGmtTime( $format='Y-m-d H:i:s' )
{
	static $time = NULL;
	if( $time === NULL )
	{
		$time = date( $format, time()-date('Z') );  // date('Z') returns the offset from GMT in seconds
	}
	return $time;
}

function getZeroPaddedNumber( $num, $size=12 )
{
	$format = '%1$0' . $size . 'd';  // $size digit number with preceding 0s
	return sprintf( $format, $num );
}

/**
 * Only does an html_escape and echos the given data. This will also replace ' (single quote) with an HTML code.
 * @param string $data
 * @param string $defVal  Value to use if( $data == '' )
 	*/
function echoCleanedData( $data, $defVal='' )
{
	$val = $data == '' ? ($defVal != '' ? $defVal : '') : $data;
	echo html_escape($val);
}

function echoJsConstants()
{
	echoJsVar('praCMS_version', '1.0' );
}

/**
 * Will echo javascript code to declare a var with the name $varName and with value $val.
 * Will treat all values as strings. Will escape single quotes, & will perform an
 * html_escape on the value. Also makes html_encoding transparent as in you can simply use the variable
 * without having to decode anything.
 *
 * @param string $varName 	The JS variable name
 * @param string $val		The var value
 * @param string $defVal  	Value to use if( $val == '' )
 */
function echoJsVar( $varName, $val, $defVal='', $useVar=false, $debug=false )
{
	$val = $val === '' ? ($defVal !== '' ? $defVal : '') : $val;
	if( $debug )
	{
		echo ( "val=$val.");
		die;
	}
	echo ($useVar?"var ":''),"$varName = $('<div/>').html('", html_escape($val), "').text();", NEWLINE;
}

function storeUserDetailsInSession( $data=array() )
{
	$ci = 	$GLOBALS['CI_INSTANCE'];
	foreach( $data as $key => $value )
	{
		$ci->session->set_userdata( $key, $value );
	}
}

function getUserDetailInSession( $key )
{
	$ci = 	$GLOBALS['CI_INSTANCE'];
	return $ci->session->userdata( $key );
}

function echoJSON( $params=array() )
{
	if ( is_array($params) )
	{
		if ( !isset($params['message']) )
			$params['message'] = "Message wan't set";
		if ( !isset($params['status']) )
			$params['status'] = 0;
	}
	else
	{
		$params = array( 'status' => 0, 'message' => $params );
	}
	echo json_encode($params);
}

function isAdminLoggedIn()
{
// 	echo "abcd: ", getUserDetailInSession('uid'), '.'; die;
	return getUserDetailInSession('uid') > 0;
}

function getProductDirPathFromProdId( $prodId )
{
	$tmp_arr = str_split( getZeroPaddedNumber($prodId), 3 );

	$path = MY_BASEPATH . 'uploads/products/' . implode('/', $tmp_arr);
	return $path;
}

function getProductUrlFromProdId( $prodId )
{
	$tmp_arr = 		str_split( getZeroPaddedNumber($prodId), 3 );

	$url = 		BASE_URL . 'uploads/products/' . implode('/', $tmp_arr);
	return $url;
}

function getProductImageFromProdId( $prodId )
{
	$fileName = 		getProductDirPathFromProdId($prodId) . '/img1.jpg';
	$imgUrl = 			getProductUrlFromProdId($prodId) . '/img1.jpg';
	
	if( !file_exists($fileName) )	// If the image doesn't exist, return a default image
	{
		$imgUrl = 	BASE_URL . '/public/images/logo.png';
	}
	return $imgUrl; 
}

/**
 * Sets a variable named status_message that you can pull using getErrorMessage
 *
 * @param string $message	The error message
 * @param string $type		'flashdata' [default], 'global'
 */
function setErrorMessage( $message, $type='flashdata' )
{
	$type = 	strtolower($type);
	$ci = 		$GLOBALS['CI_INSTANCE'];
	if ( $type == 'flashdata' )
	{
		$ci->session->set_flashdata( 'status_message', $message );
	}
	elseif ( $type == 'global' )
	{
		$GLOBALS['status_message'] = $message;
	}
}

/**
 * Pulls the status_message that was set using setErrorMessage
 * $type: detect, flashdata, merge, & 'all others'
 * 			detect will first try flashdata & if not found will try $GLOBALS
 * 			merge will first pull from flashdata and then append message (if any) from $GLOBALS
 */
function getErrorMessage( $type='detect' )
{
	$type = 	strtolower($type);
	$ci = 		$GLOBALS['CI_INSTANCE'];
	$message = 	'';

	if( $type == 'detect' )
	{
		$message = 		$ci->session->flashdata( 'status_message' );
		$message2 = 	isset($GLOBALS['status_message']) ? $GLOBALS['status_message'] : '';
		if ( $message != '' && $message != null && $message2 != '' && $message2 != null )	// Merge the two messages
			$message .= $message2;
		elseif( $message2 != '' && ($message == null || $message == '') )
		$message = $message2;
	}
	elseif ( $type == 'flashdata' )
	{
		$message = 		$ci->session->flashdata( 'status_message' );
	}
	elseif( $type == 'global' )
	{
		$message = 		isset($GLOBALS['status_message']) ? $GLOBALS['status_message'] : '';
	}
	elseif( $type == 'merge' )
	{
		$message = 		$ci->session->flashdata( 'status_message' );
		$message .= 	isset($GLOBALS['status_message']) ? $GLOBALS['status_message'] : '';
	}

	return $message;
}
