<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		
		if( !isAdminLoggedIn() )
			die("You have to login to view this page");
	}
	
	public function index()
	{
		$viewData = 		array('pageTitle' => 'Welcome to PraCMS Admin Section');
		$this->load->view( 'admin/home', $viewData );
	}
	
	public function newproduct()
	{
		if ( $this->input->post('btnSubmit') )		// If user has submitted form for creating a new product
		{
			$this->load->model('product_model');
			$res = 		$this->product_model->createProduct();
			if( $res['status'] )	// If the product was created successfully
			{
				setErrorMessage( 'Your product was posted successfully!', 'global' );
			}
			else
			{
				setErrorMessage( $res['message'], 'global' );
			}
// 			$this->redirect('/admin/newproduct');
		}
		
		$prodDetails = 		array( 'prod_id' => 0, 'title' =>'', 'price'=> '', 'description'=>'', 'img1'=>'');
		$viewData = 		array('pageTitle' => 'Create a new product', 'product' => $prodDetails );
		$this->load->view( 'admin/create_product', $viewData );
	}
	
	public function validateProduct()
	{
		$this->form_validation->set_rules('txtTitle', 'Title', 'required');
		$this->form_validation->set_rules('txtPrice', 'Price', 'required');
		$this->form_validation->set_rules('txaDescription', 'Description', 'required');
				
		$this->form_validation->set_message('required', ' %s is required'  );
		
		return $this->form_validation->run();
	}
	
	// Ajax calls to this function
	public function disableproduct( $prodId=0 )
	{
		$this->load->model('product_model', 'obj');
		echoJSON( $this->obj->disableProduct( $prodId ) );
	}
	
	// Ajax calls to this function
	public function enableproduct( $prodId=0 )
	{
		$this->load->model('product_model', 'obj');
		echoJSON( $this->obj->enableProduct( $prodId ) );
	}
	
	public function editproduct( $prodId=0 )
	{
		if ( $this->input->post('btnSubmit') )		// If user has submitted the form for editing a new product
		{
			$this->load->model('product_model');
			$res = $this->product_model->editProduct( $prodId );
			if( $res['status'] )	// If the product was created successfully
			{
				setErrorMessage( 'Your product was edited successfully!', 'global' );
			}
			else
			{
				setErrorMessage( $res['message'], 'global' );
			}
			// 			$this->redirect('/admin/newproduct');
		}
		//
		
		$this->load->model('product_model');
		$prodDetails = 		$this->product_model->getProductDetails( $prodId );
		if( !$prodDetails )
		{
			die("Sorry the product does not exist. Hit back to return to where you were.");
		}
		$viewData = 		array('pageTitle' => 'Create a new product', 'product' => $prodDetails );
		$this->load->view( 'admin/create_product', $viewData );
	}
	
}
