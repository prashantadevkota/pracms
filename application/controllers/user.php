<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

    protected $viewData = array();

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }//end __construct()

	public function ajax_trylogin()
	{
		$username = $this->input->post('txtUserName', TRUE);
		$password = $this->input->post('txtPassword', TRUE);

		$this->load->model('user_model');
		$data = $this->user_model->tryLogin( $username, $password );
		echoJSON( $data );
	}

	public function ajax_logout()
	{
		storeUserDetailsInSession( array('uid' => 0) );
		echoJSON( array('status'=>1, 'message'=>'Logout was successful') );
	}
}