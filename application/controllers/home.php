<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()
	{
		$this->load->model('product_model');
		$sortBy = 			isset( $_GET['sortby'] ) ? $_GET['sortby'] : 'time';
		$flgListAll = 		isAdminLoggedIn() ? true : false;
		
		$list = 			$this->product_model->getList( $sortBy, $flgListAll );
		$viewData = 		array('pageTitle' => 'Welcome to PraCMS', 'sortBy' => $sortBy, 'list' => $list);
		$this->load->view( 'home/home', $viewData );
	}
}
