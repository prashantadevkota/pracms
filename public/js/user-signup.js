var 
	frmUserSignup = 		'frmUserSignup'		// This is the name of the User Signup form
	,frmChurchSignUp=		'frmChurchSignUp'
	,emailVerifyDivId = 	'divConfirmEmail'
	,emailSpanId = 			'spEmailVerifyD1'
	,userRegDiv = 			'divUserRegister'
	,msgDivId = 			'divMsg'
;

function verifyEmail()
{
//	msgBox('this is a test message');
	var 
		span = 					document.getElementById(emailSpanId)
		,email = 				document.getElementById('txtEmail').value
		,retval = 				0
	;

	// First try and validate the form
	var res = $("#"+frmUserSignup).valid();		// Use jquery validation
	if ( res )	// If the form validates successfully, the user has to verify their email
	{
		// All this is to update the email_id in the SPAN
		while( span.firstChild )
		{
		    span.removeChild( span.firstChild );
		}
		span.appendChild( document.createTextNode( '(' + email + ')' ) );
		// END_OF All this is to update the email_id in the SPAN
	
		showConfirmEmailDiv();
	}
}

function btnChange_VE_Clicked()
{
	hideConfirmEmailDiv();
}

function btnContinue_VE_Clicked()
{
	hideConfirmEmailDiv();
	signUp();
}

function hideConfirmEmailDiv()
{
	// Hide the div to verify the email of the user and return them to the form
	document.getElementById(emailVerifyDivId).style.display = 	'none';
	document.getElementById(msgDivId).style.display = 			'none';
	document.getElementById(userRegDiv).style.display = 		'block';
}

function showConfirmEmailDiv()
{
	// Now show the div to verify the email of the user
	document.getElementById(emailVerifyDivId).style.display = 	'block';
	document.getElementById(msgDivId).style.display = 			'none';
	document.getElementById(userRegDiv).style.display = 		'none';
}

function showMessage( message )
{
	hideConfirmEmailDiv();
	document.getElementById(msgDivId).innerHTML = 				
		'<input type="button" onClick="javascript:hideMessageDiv();" value="X"> <br/>' + message;
	showMessageDiv();
}

function showMessageDiv()
{
	// Show the div that displays the message
	document.getElementById(emailVerifyDivId).style.display = 	'none';
	document.getElementById(msgDivId).style.display = 			'block';
	document.getElementById(userRegDiv).style.display = 		'none';
}

function hideMessageDiv()
{
	// Hide the div that shows the message
	document.getElementById(emailVerifyDivId).style.display = 	'none';
	document.getElementById(msgDivId).style.display = 			'none';
	document.getElementById(userRegDiv).style.display = 		'block';
}

function signUp()
{
	var 
		url = 		BASE_URL+'user/ajax_signupsubmit'
		,data = 	new Object()
	;
	
	data.txtFirstName = 	$("#txtFirstName").val();
	data.txtLastName = 		$("#txtLastName").val();
	data.txtEmail = 		$("#txtEmail").val();
	data.txtPassword = 		$("#txtPassword_S").val();
	data.txtPassword2 = 	$("#txtPassword_S2").val();
	postUsingAjax( url, data, 'afterSignUp', 'POST' );
}

function afterSignUp( status, message )
{
	if( status )	// If the signup was successful
	{
		document.getElementById(frmUserSignup).reset();
		if ( ENVIRONMENT == "development" )
		{
			showMessage( message.activation_link );
		}
		else
		{
			var signMessage = 'Please check your email for a validation link. Check your SPAM folder if the message does not show up in 3 minutes.';
			msgBox(signMessage);
		}
	}
	else	// The signup was NOT SUCCESSFUL
	{
		alert( message );
	}
}

$().ready( function() 
{
	$("#txtPassword").keypress(function(event) 
	{
	    if (event.which == 13) 
	    {
	        event.preventDefault();
	        tryAjaxLogin();
	    }
	});
	
	$("#txtPassword_S2").keypress(function(event) 
			{
			    if (event.which == 13) 
			    {
			        event.preventDefault();
			        verifyEmail();
			    }
	});
});

function btnChurchJoinNow_clicked()
{
	var res = $("#"+frmChurchSignUp).valid();		// Use jquery validation
	if ( !res )	// If the form does not validate
	{
		return false;
	}
	
	var 
		churchName = 			$("#txtChurchName").val()
		,churchCity = 			$("#txtChurchCity").val()
		,churchState = 			$("#selChurchState").val()
		,url = 					BASE_URL + 'church/ajax_signupsearch'
		,callback_function = 	'afterChurchSignUpSearch'
		,method = 				'POST'
		,data = 				new Object()
	;
	data.txtChurchName = 		churchName;
	data.txtChurchCity = 		churchCity;
	data.txtChurchState = 		churchState;
	
	postUsingAjax( url, data, callback_function, method );
	return false;
} 

// Callback function
function afterChurchSignUpSearch( status, message, data )
{
	if( status == 0 )	// If the church wasn't found, allow the user to create a new Church
	{
		document.getElementById('frmChurchSignUp').submit();
	}
	else if ( status == 1 )	// If the call was successful(perfect single match was found)
	{
		var msg= "This church is pre-loaded. Are you the Church Admin for this Church?";
		yesNoBox(msg, 'redirectToChurchSignupAsAdmin', data );
	}
	else if ( status == 2 )	// If Church already approved
	{
		alert(message);
	}
	else if ( status == 3 )	// If multiple records found
	{
		var form1 = document.getElementById('frmChurchSignUp');
		form1.action = BASE_URL + 'church/searchresults';
		form1.submit();	// Uncomment this after finishing testing
	}
}

function redirectToChurchSignupAsAdmin( ans, data )
{
	if (ans == 1)
	{
		document.location =  BASE_URL + 'church/signup/' + data.church_id;
	}
}