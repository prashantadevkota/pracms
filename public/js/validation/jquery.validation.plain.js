﻿//var formNameID="RequestedFormDatasss";
//var imgLoading = siteRelativePath + "icons/loading.gif";
var isValid = false;

(function ($) {
    $.fn.FormValidationPlain = function (options) {
		 var defaults = {            
            showError : true,
			errorClass : "error"
			
           
        };
        var opts = $.extend(defaults, options);
		var validator = $(this).validate({
        onsubmit: false,
        rules: {},
		messages: {},
//		errorClass: "input-validation-error",
		errorClass: opts.errorClass,
		validClass:'success',
    	errorElement:'label',
		errorPlacement: function(error, element) {
			/*var errors = validator.numberOfInvalids();
			var errMsg = "<strong>" + errors + "</strong> "+ "invalid field(s) are found. Please fix them first in order to submit your request.";
			if (validator.numberOfInvalids() > 0) {
				$("#myErrorBox").html(errMsg).show();
			} else {
				$("#myErrorBox").hide();
			}*/
			if(opts.showError == true){
				//error.insertAfter( element.siblings("em") );			
				error.appendTo(element.siblings("em") );
			
			}
		},
		 highlight: function (element, errorClass, validClass) { 
			$(element).parents("div.control-group").addClass(errorClass).removeClass(validClass); 
		}, 
		unhighlight: function (element, errorClass, validClass) { 
			$(element).parents("div.error").removeClass(errorClass);//.addClass(validClass); 
		},
		success: function(label) {
			// set &nbsp; as text for IE
			//label.html("nbsp;dddddddddddddddone").addClass("checked");
			//label.remove();
		}
		//,debug:true
	});

/***************************** End Form Validation *************/

	};
})(jQuery);  


function SubmitForm(frmID) 
{	
    var isValid = $("#"+frmID).valid();
    if (isValid) return true;
    return false;
}

function SubmitFormConfirm(frmID, mode) 
{	
	var alertMsg = "";
    if (mode == "N") {
        alertMsg = "'Are you sure to save the details?'";
    }
    var isValid = $("#"+frmID).valid();
	 if (isValid && alertMsg != "") {
        if (confirm(alertMsg)) {            
            return true;
        }
        else {            
        	return false;
    	}
 	} 
    return false;   
}
function DeleteConfirm(delUrl, delSNo, msg) {

    alertMsg = (msg == '' || msg == undefined ? "Are you sure to delete the record?" : msg);
    if (confirm(alertMsg)) {
        JqueryDelete(delUrl, delSNo);
    }
    else {
        return false;
    }
}

function ShowAjaxLoading(thisElmId) { 
    $("#"+thisElmId).empty().append('<div style="text-align: center;"><img alt="Loading..." src="'+base_url+'/images/icons/indicator-big.gif"></div>');
}
function HideAjaxLoading(thisElmId) {
    $("#" + thisElmId).empty();
}

function ShowSiblingMessage(elmIdSibling, msg) {
    var objL = $("#" + elmIdSibling).siblings(".error");
    if (objL.length >= 1) {
        $("#" + elmIdSibling).siblings("span.error").html(msg);
    } else {
        $("#" + elmIdSibling).after('<span class="error">' + msg + '</span>');
    }
}

function DeleteTableListRow(thisElm) {
    $("#" + thisElm).css({ "background-color": "red" }).fadeOut(500, function () {
        $("#" + thisElm).remove();
    });
}

function JqueryLoadSelectOptionHtml(selValue, targetElm, urlAction)
{
    if(selValue===''){
		$("#" + targetElm).empty().append('<option value="">-- Select --</option>');
		return;
	}
    $.ajax({
        async: false,
        type: "POST",
        url: urlAction +"/"+ selValue,
        contentType: "text/html",
        beforeSend: function () {
            $("#" + targetElm).attr('disabled', 'disabled');
            $("#" + targetElm).addClass('ac_loading');
        },
        
        success: function (htmlResult) {                    
			$("#" + targetElm).empty().append(htmlResult);
			$("#" + targetElm).removeClass('ac_loading');
			$("#" + targetElm).removeAttr('disabled');
     
        }
    });
}

function jAjaxHtml(targetElm, urlAction, showLoading) {
    
    $.ajax({
        type: "POST",
        url: urlAction,
        contentType: "text/html",
        beforeSend : (showLoading == "false") ? "" : ShowAjaxLoading(targetElm),
        success: function (htmlResult) {
            $("#" + targetElm).empty().append(htmlResult);
            
        }
    });
}

function jSearchPost(thisForm, targetElm) {
    var data = "";
    data = $(thisForm).serialize();
    $.ajax({
        async: false,
        type: thisForm.method, //"POST",
        url: thisForm.action,
        data: data,
        beforeSend: ShowAjaxLoading(targetElm),
        success: function (result) {
            $("#" + targetElm).empty().append(result);
        }
    });
}

function jPostForm(thisForm, urlAction, targetElm) {
    var data = "";
    data = $(thisForm).serialize();    
	targetElm = targetElm == undefined || targetElm == "" ? "contentFormUpdatePanel" : targetElm;
    ShowLoadingBox(); // $("#ajaxLoader").show();
    $.ajax({
        async: false,
        type: "POST",
        url: urlAction,
        data: data,
        //data: ({ id: this.getAttribute('id') }),
        //completed: $unblock(),
        success: function (result) {
            //$(this).addClass("done");
            // var domElement = $(result); // create element from html
            // $("#DivFormContent").append(domElement); // append to end of list    
            HideLoadingBox();
            $("#contentFormUpdatePanel").empty().append(result);
            //$.unblockUI();
        }
    });
    HideLoadingBox();
}

function JqueryDelete(action, sno) {
    $.ajax({
        async: false,
        type: "POST",
        url: action,
        dataType: "json",
        success: function (result) {
			if (result.response_status == "true") {
                DeleteTableListRow(sno);
            } else if (result.response_status == "false") {
                alert("Error Deleting Record!\n" + result.response_message + "");

            }
        }
    });    
}

function showFlashMsg(msgType , msg) {
	var $flashDiv =  $('#flash-message-container');
	 $flashDiv.empty().append( '<div class="row-fluid"><div class="span12"><div class="alert alert-'+msgType+'">'
				+ msg +'<a href="#" data-dismiss="alert" class="close">x</a></div></div></div>' );
		$('.alert').slideDown('slow');
	
}

function hideFlashMsg() {
	$('#main-container-wrap').find('.alert').slideUp('slow', function() {
		$(this).removeClass().addClass('alert');
	});
}


jQuery(document).ready(function() {
	/**
	 * 	Static page Toggle Function make active or deactive the page
	 */
		$(document).on('click','[data-action^="toggle"]', function(e) { 	
			e.preventDefault();
			var 
			$this = $(this),
			$href = $this.attr('href'),
			$text 	=	$this.text(),
			$action = $this.attr('data-action'),
			$id = $this.attr('data'),
			$targetElmDelete     = $("#"+$this.attr('data-target')),
			$status     = $this.attr('data-status'),		
			$url = $href;			 	
			//$status	=	($text.toLowerCase() == 'disable') ? '0' : '1';
		
			if (confirm( 'Are you sure?' )) {
				$.ajax({
					url		:	$url,
					type	:	'post',
					data	:	{id : $id, status : $status},
					dataType:	'json',
					success	:	function (result) {
						if ($status == '1') {
							$this.text('Disable');
							$this.attr('data-status','0'); 
							$this.parent().parent().removeClass('warning').addClass('success');
						} else {
							$this.text('Enable');
							$this.attr('data-status','1'); 
							$this.parent().parent().removeClass('success').addClass('warning');
						}
		
						showFlashMsg(result.response_status, result.response_message);
					},
					error	:	function (jqXHR, textStatus, errorThrown) {
						 console.log(jqXHR);
					}
				});
			}
		 });
		
		$(document).on('click','[data-action^="deleterow"]', function(e) { 	
			e.preventDefault();
			var 
			$this = $(this),
			$href = $this.attr('href'),
			$text 	=	$this.text(),
			$action = $this.attr('data-action'),
			$id = $this.attr('data'),
			$targetElmDelete     = $("#"+$this.attr('data-target')),
			$status     = $this.attr('data-status'),		
			$url = $href;			 	
			//$status	=	($text.toLowerCase() == 'disable') ? '0' : '1';
		
			if (confirm( 'Are you sure to delete this record?' )) {
				$.ajax({
					url		:	$url,
					type	:	'post',
					data	:	{id : $id, status : $status},
					dataType:	'json',
					success	:	function (result) {
						if (result.response_status == "true") {
							$this.parent().parent().removeClass().addClass('alert-danger').fadeOut(500, function () {
						        $($targetElmDelete).remove();
							});
							showFlashMsg('success', result.response_message);
			            } else if (result.response_status == "false") {
			               // alert("Error Deleting Record!\n" + result.response_message + "");
			            	showFlashMsg('danger', result.response_message);
			            }		
						
					},
					error	:	function (jqXHR, textStatus, errorThrown) {
						 console.log(jqXHR);
					}
				});
			}
		 });
});

//$(window).load(function() {
//    // run code
//    HideEmOnload();
//});

