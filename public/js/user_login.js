function forgotPassword()
{
	var
		 url = BASE_URL + 'user/ajax_forgotPassword'
		,data = new Object()
	;
	data.txtUserName = 	document.getElementById('txtUserName').value;

	if( data.txtUserName == '' )
	{
		var message = 'Please provide your username.';
		showErrorMessage( message, 'spnUserName', 'txtUserName' );
		//alert('Please provide your username.');
	}
	else
	{
		postUsingAjax( url, data, 'afterForgotPassword', 'POST' );
	}
}

function afterForgotPassword( status, message )
{
	if ( status )	// If the forgot-password sequence was successful
	{
		//alert('A password reset link has been sent to your email address.');
		var message = 'A password reset link has been sent to your email address.';
		showErrorMessage( message, 'spnUserName', 'txtUserName' );
	}
	else
	{
		//alert( "ERROR! " + message );
		showErrorMessage( message, 'spnUserName', 'txtUserName' );
	}
}

function tryAjaxLogin()
{
	var
		 url = BASE_URL + 'user/ajax_tryLogin'
		,data = new Object()
	;
	data.txtUserName = 		document.getElementById('txtUserName').value;
	data.txtPassword = 		document.getElementById('txtPassword').value;
	data.chkRememberMe = 	document.getElementById('chkRememberMe').checked ? 1 : 0;
	data.timeZoneName = 	timeZoneName;
	if( data.txtUserName == '' || data.txtPassword == '' )
	{
		var message = 'Please enter a username and password before trying to login.';
		showErrorMessage( message, 'spnUserName', 'txtUserName' );
		//alert('Please enter a username and password before trying to login.');
	}
	else
	{
		postUsingAjax( url, data, 'afterTryAjaxLogin', 'POST' );
	}
	return false;
}

// Call-back function that is called after AJAX login attempt
function afterTryAjaxLogin( status, message, data )
{
	if ( status )	// If the login was successful
	{
		var role_id = 0;
		if( data.hasOwnProperty('role_id') )
			role_id = data.role_id;

		if ( role_id == 2 )		// Normal User
			document.location = BASE_URL + 'user/home';
		else if ( role_id == 3 )	// Church Admin
			document.location = BASE_URL + 'church/home';
		else
			msgBox('Admins have to use the admin login. Login through the user-login is not allowed.');
	}
	else		// If the login attempt was unsuccuessful
	{
		//alert( "ERROR! " + message );
		showErrorMessage( message, 'spnUserName', 'txtUserName' );
	}
}

function showErrorMessage( errorMessage, parentElementId, forElementId )
{
//console.log('inside showErrorMessage');
	var
		a = $("label[for='" + forElementId + "']")
		, flgLabelExists = (typeof a[0] != 'undefined' )
		, parent = document.getElementById(parentElementId)
		;
//console.log('flgLabelExists=' + flgLabelExists);
	$("label[class='error-text']").css('display','none');	// First hide all error messages

	if ( !flgLabelExists )	// If the label doesn't exist, create it
	{
//console.log('Inside if( !flgLabelExists), in other words label does not exist');
		var newLabel = 		document.createElement('label');
		var labelStyle = 	'display:block;';
		newLabel.id = 		'lblLoginErrorMessage';
		newLabel.innerHTML = errorMessage;
		newLabel.setAttribute('class', "error-text2");
		newLabel.setAttribute('for', forElementId);
		//newLabel.setAttribute('display', 'block');
		newLabel.setAttribute('style', labelStyle);
		parent.appendChild(newLabel);
	}
	else	// If the label already exists
	{
//console.log('Inside else');
		$(a).text(errorMessage);
		$(a).css('display','block');
	}
}