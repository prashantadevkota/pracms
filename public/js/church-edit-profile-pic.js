var 
	profPic = 				'profile'
	,profLogo = 			'logo'
	,frmEditProfilePic = 	'frmEditProfilePic'
;

function verifyPictures()
{
	var 
		profPicVal = 		document.getElementById(profPic).value
		,profLogoVal = 		document.getElementById(profLogo).value
	;
	
	var 
		flgError = 0
		,allowedExtsArr = ['jpg','jpeg','gif','png']
		,allowedExtsStr = 'JPG, GIF or PNG'
	;
	
	if ( profLogoVal !='' && !isPic_JPG_GIF_PNG(profLogoVal) )
	{
		alert( "Logo must be either " + allowedExtsStr );
		flgError = 1;
	}
	
	if ( flgError == 0 &&  profPicVal !='' && !isPic_JPG_GIF_PNG(profPicVal) )
	{
		alert( "The profile picture must be either " + allowedExtsStr );
		flgError = 1;
	}
	
	if ( profPicVal == '' && profLogoVal == '' )
	{
		alert('Nothing to submit!');
		flgError = 1;
	}
	
	var 
		picElementsList = ['profile','logo']
		picSelectedCount = 0
		;

	for(var i=0; i<picElementsList.length; i++)
	{
	  if( $( "#" + picElementsList[i]).val() != '' )  // Has a value
	  {
	    picSelectedCount++;
	  }
	}

	if( picSelectedCount > 1 ) 
	{
		flgError = 1;
		msgBox('You can select only one file at a time');
	}
	
	if ( !flgError )	// If there was no error
	{
		document.getElementById(frmEditProfilePic).submit();
	}
}

function isPic_JPG_GIF_PNG( filename )
{
	var retval = 0;
	var tmpArr = filename.split('.');
	
	if ( tmpArr.length > 1 )	// If the filename does have an extension
	{
		var ext = tmpArr.pop().toLowerCase();
		if ( ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' )	// Allowed extensions
		{
			retval = 1;
		}
	}
	return retval;
}

function deletePic( picType )
{
	var 
		url = 		BASE_URL+'church/ajax_deletepicture'
		,data = 	new Object()
	;

	ans = confirm('Are you sure you want to delete this picture?');
	if ( ans )	// If the user confirmed deletion
	{
		data.picType = 	picType;
		data.churchId = document.getElementById('hidChurchId').value;
		postUsingAjax( url, data, 'afterDeletePic', 'POST' );
	}
}

function afterDeletePic( status, message )
{
	if ( status )	// If deletion was successful
	{
		location.reload();
	}
	else		// Deletion failed
	{
		errMsg = message;
		alert(errMsg);
	}
}

function clearAllSelectedPics()
{
	var 
		picElementsList = ['profile','logo']
		;
	for(var i=0; i<picElementsList.length; i++)
	{
		$("#" + picElementsList[i]).val('');
	}
}