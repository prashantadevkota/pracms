var folder_structure = [{"Inbox":"1","Sent":"2","Trash":"3","Archive":"4","Deleted":"5"}];
var folder_structure_property = {"1":{"id":"1","norecordmessage":"No new messages"},"2":{"id":"2","norecordmessage":"No sent messages"},"3":{"id":"3","norecordmessage":"No deleted messages"},"4":{"id":"4","norecordmessage":"No archived messages"},"5":{"id":"5","norecordmessage":"No deleted messages"}};

var base_url ='/';
var totalSelChkbox = 0;
$(document).ready(function(){


	$(".clsMsgList-Row").on({
		"mouseover" :function(e){
			//alert(this.attr('id'))
			//$(this).css("background-color","#EBEBEB");
		},
		"mouseout" : function(e){
			//$(this).css("background-color","#FFF");
		},
		"click" : function(e){
			//$(".clsMsgList-Row").removeClass("clsMsgList-Selected").not(this);
			//$(this).addClass("clsMsgList-Selected");
		}

	});

	$(document).on("click",".clsMsgList-Col",function(e){

			var thisParent = $(this).parent("tr");
			if(thisParent.hasClass('clsMsgList-Selected')){return false;}
			var rowId = thisParent.attr("id");
			var msgTicketId = thisParent.attr("ticket");
			var msgFolderId = thisParent.attr("folder");
			var msgId = rowId.split('_')[1];
			if(msgId > 0 && msgTicketId != '' )
			{
				var url = base_url+"message/messagereadbatch/"+msgId+"/"+msgTicketId+"/"+msgFolderId;
				JqueryAjaxHtml("message_reader", url);
				thisParent.removeClass('msg-unread');
				$(".clsMsgList-Row").removeClass("clsMsgList-Selected");
				thisParent.addClass("clsMsgList-Selected");

			}
	});

	$(document).on( "click",".clsMsgList-Col-Highlight",function(e){
			//alert(this.attr('id'))
			var thisParent = $(this).parent("tr");
			var rowId = thisParent.attr("id");
			var msgTicketId = thisParent.attr("ticket");
			var msgFolderId = thisParent.attr("folder");
			var msgId = rowId.split('_')[1];
			if(msgId > 0 && msgTicketId != '' )
			{
				document.location.href = base_url+"message/messagereadbatchfull/"+msgId+"/"+msgTicketId+"/"+msgFolderId;
			}

	});

	$(document).on("click","span.flag-cont", function(){

		var $this = $(this);
		var thisParent = $this.parents("tr");
		var rowId = thisParent.attr("id");
		var msgTicketId = thisParent.attr("ticket");
		var msgId = rowId.split('_')[1];
		var flagValue = 0;
		if($this.hasClass('flag-u')){ flagValue = 3;}else{flagValue = 4;}
		var data = {'message_id' : msgId,  'message_ticket' : msgTicketId, marker_type : flagValue};
		var action = "/message/setmessageflag"+'/'+msgId;
		$.ajax({
			async: false,
			type: "POST",
			url: action,
			data: data,
			success: function (result) {
				// {"ErrNumber":0,"ErrMessage":"","ErrSource":"","ErrType":null,"CheckResponseStatus":true}
				var obj = $.parseJSON(result);

				if (obj.response_status == "true") {
					if($this.hasClass('flag-u'))
					{
						$this.removeClass("flag-u").addClass('flag-f');
					}else
					{
						$this.removeClass("flag-f").addClass('flag-u');
					}

				} else if (obj.response_status == "false") {
					alert(obj.response_message);

				}
			}
		});
		return false;


	});

	$(document).on("change","#MsgBulkMarkerStatus", function(){
		countCheckedBox();

		var $this = $(this);
		var thisSelectedValue = this.value;
		var thisSelectedText = $this.find("option:selected").text();
		if(thisSelectedValue == 0 || thisSelectedValue == "" || thisSelectedValue == undefined )return false;

		if(totalSelChkbox > 0){

			var data = $("#FormMessageList").serialize();
			var action = "/message/messagemarkerbulk/"+thisSelectedText;

			$.ajax({
				async: false,
				type: "POST",
				url: action,
				data: data,
				beforeSend: function () {
					$this.attr('disabled', 'disabled');
					$this.addClass('ac_loading');
				},
				success: function (result) {
					// {"ErrNumber":0,"ErrMessage":"","ErrSource":"","ErrType":null,"CheckResponseStatus":true}
					var obj = $.parseJSON(result);
					if (obj.response_status == "true") {
						$(" input[type=checkbox][name*='chkBoxChildSelected']:checked").each(function(){
							ChangeMessageMarker("msglist_"+this.value, thisSelectedValue);
						});
						$this.find("option[value='0']").prop('selected', true);

					} else if (obj.response_status == "false") {
						alert(obj.response_message);

					}
				}
			});
			$this.removeAttr('disabled');
			$this.removeClass('ac_loading');
			return false;
		}else
		{
			alert("Please select message to "+thisSelectedText);
			$this.find("option[value='0']").prop('selected', true);

		}
		return false;
	});

	$(document).on('click',"#btnSubmitMsgDelete",function(){
		var confirmMsg, successMsg ;
		var msgId = $("#view_message_id").val();
		if( $("#view_message_folder_id").val() == '3')
		{
			confirmMsg = "Are you sure to delete the selected message?";
			successMsg = "Message successfully deleted.";

		}else
		{
			confirmMsg = "Are you sure you want to delete selected message(s)?";
			successMsg = "Message(s) successfully sent to trash.";
		}

		if(confirm(confirmMsg))
		{

			var action = "message/delete/"+'/'+$("#view_message_id").val()+'/'+$("#view_message_folder_id").val();
			$.ajax({
				async: false,
				type: "POST",
				url: action,
				success: function (result) {
					// {"ErrNumber":0,"ErrMessage":"","ErrSource":"","ErrType":null,"CheckResponseStatus":true}
					var obj = $.parseJSON(result);
					if (obj.response_status == "true") {
						alert(successMsg);
						var cntMsgRow = $("#msglist_"+msgId).parent().children('tr').not("#msglist_"+msgId).length;
						DeleteTableListRow("msglist_"+msgId);
						ResetMessangerReader("msglist_"+msgId,cntMsgRow);
						/*$("#message_reader").empty().html('<div class="white_pad wdt850 default-select-info">'
							+(cntMsgRow >1 ? "Click on a message to view it here" : "No new messages")
							+'</div>');*/

					} else if (obj.response_status == "false") {
						alert(obj.response_message);

					}
				}
			});

		}
		return false;

	});

	$(document).on("click","#btnSubmitMsgBulkDelete", function(){
		countCheckedBox();
		var confirmMsg, successMsg ;
		if( $("#message_folder_id").val() == '3')
		{
			confirmMsg = "Are you sure to delete the selected message?";
			successMsg = "Message successfully deleted.";

		}else
		{
			confirmMsg = "Are you sure you want to delete selected message(s)?";
			successMsg = "Message(s) successfully sent to trash.";
		}

		if(totalSelChkbox > 0){
			if(confirm(confirmMsg))
			{

				var data = $("#FormMessageList").serialize();
				var action = "/message/deletebulk/"+$("#message_folder_id").val();
				$.ajax({
					async: false,
					type: "POST",
					url: action,
					data: data,
					success: function (result) {
						// {"ErrNumber":0,"ErrMessage":"","ErrSource":"","ErrType":null,"CheckResponseStatus":true}
						var obj = $.parseJSON(result);
						if (obj.response_status == "true") {

							var cntMsgRow = $("#msg_table tr").length - totalSelChkbox;
							$(" input[type=checkbox][name*='chkBoxChildSelected']:checked").each(function(){
								DeleteTableListRow("msglist_"+this.value);
								ResetMessangerReader("msglist_"+this.value,cntMsgRow);
							});
							alert(successMsg);
						}else if (obj.response_status == "false") {
							alert(obj.response_message);

						}
					}
				});

			}
			return false;
		}else
		{
			alert("Please select message to delete");

		}
		return false;
	});

//	totalSelChkbox = $(".CheckboxToggle").checkboxToggle({
//		parentChkbox : "#chkBoxParentSelected",
//		childChkbox : "chkBoxChildSelected",
//		onclick : function(){
//			alert('a');
//		}
//
//	});

});
function ResetMessangerReader(thisElmId , cntMsgRow)
{
	var noMessage = "No new messages";
	if($("#message_folder_id").val() == "1")
	{
		noMessage = folder_structure_property["1"]["norecordmessage"];

	}else if($("#message_folder_id").val() == "2")
	{
	 	noMessage = folder_structure_property["2"]["norecordmessage"];

	}else if($("#message_folder_id").val() == "3")
	{
		noMessage = folder_structure_property["3"]["norecordmessage"];
	}else
	{
		noMessage = "No new messages";
	}

	if($("#"+thisElmId).hasClass('clsMsgList-Selected') || cntMsgRow==1	){
		$("#message_reader").empty().html('<div class="white_pad wdt850 default-select-info">'
		+(cntMsgRow >1 ? "Click on a message to view it here" : noMessage)
		+'</div>');
	}
}
function DeleteMessageConfirm(delUrl, delElmId) {

    alertMsg = "Are you sure to delete this message?";
    if (confirm(alertMsg)) {
        jqueryDeleteMessage(delUrl, delElmId);
    }
    else {
        return false;
    }
}
function jqueryDeleteMessage(action, delElmId) {
    var data = "";
    $.ajax({
        async: false,
        type: "POST",
        url: action,
        data: data,
        contentType: "text/json",
        success: function (result) {
            // {"ErrNumber":0,"ErrMessage":"","ErrSource":"","ErrType":null,"CheckResponseStatus":true}
        	var obj = $.parseJSON(result);
            if (obj.response_status == "true") {
				var cntMsgRow = $("#msg_table tr").not("#"+delElmId).length;
                DeleteTableListRow(delElmId);
				ResetMessangerReader(delElmId, cntMsgRow);

            } else if (obj.response_status == "false") {
                alert(obj.response_status+"\n");

            }
        }
    });
}
function ChangeMessageMarker(thisElmId, markerType)
{
	if(markerType == 1)
	{
		$("#"+thisElmId+"  ").removeClass("msg-unread");
	}else if(markerType == 2)
	{
		$("#"+thisElmId+"  ").addClass('msg-unread');
	}else if(markerType == 3)
	{
		$("#"+thisElmId+" :nth-child(3) ").children('span.flag-cont').removeClass("flag-u").addClass('flag-f');
	}else if(markerType == 4)
	{
		$("#"+thisElmId+" :nth-child(3) ").children('span.flag-cont').removeClass("flag-f").addClass('flag-u');
	}
}
function countCheckedBox()
{
	totalSelChkbox = $(" input[type=checkbox][name*='chkBoxChildSelected']:checked").length;
	return totalSelChkbox;
}
    $(function() {
       	/*	var subject = $("#subject"),
            message = $("#message"),
            receiver_id = $("#receiver_id"),
            allFields = $( [] ).add( subject ).add( message ).add( receiver_id );

        $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 300,
            width: 550,
            modal: true,
            buttons: {
                "Reply": function() {
					 var data = "";
				     data = $("#formReply").serialize();
                     $.ajax({
						async: false,
						type: "POST",
						url: $("#formReply").attr("action"),
						data: data,
						success: function (result) {
							$("#dialog-form").dialog("close");
							$("#dialog-success").dialog("open");
							JqueryAjaxHtml('country2','<?=site_url('ajax/messageSentList/');?>');
						},
						error: function(result){
							alert(result);
					 	}
					});

                },
                Cancel: function() {
                    $( this ).dialog("close");
                }
            },
            close: function() {
                allFields.val("").removeClass("ui-state-error");
            }
        });

		$("#dialog-success").dialog({
            autoOpen: false,
            height: 120,
            width: 450,
            modal: true,
			buttons: {
				Close: function() {
					$(this).dialog("close");
				}
			}
        });

		$("#dialog-view-message").dialog({
            autoOpen: false,
            height: 300,
            width: 450,
            modal: true,
			buttons: {
				Close: function() {
					$(this).dialog("close");
				}
			}

        });

        $( ".reply-message" ).on('click', function() {
			var receiverId = $(this).attr('replyTo');
			var subject = $(this).attr('subject');
			$("#receiver_id").val(receiverId);
			$("#subject").val(subject);
			$("#dialog-form").dialog("open");
        });*/


    });

