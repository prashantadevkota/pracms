var
	profPicMain = 			'profile'
	,profPic1 = 			'profile1'
	,profPic2 = 			'profile2'
	,profPic3 = 			'profile3'
	,profPic4 = 			'profile4'
	,frmEditProfiePic = 	'frmEditProfiePic'
;

function verifyPictures()
{
	var
		profPicMainVal = 	document.getElementById(profPicMain).value
		,profPic1Val = 		document.getElementById(profPic1).value
		,profPic2Val = 		document.getElementById(profPic2).value
		,profPic3Val = 		document.getElementById(profPic3).value
		,profPic4Val = 		document.getElementById(profPic4).value
	;

	var maxSizeInKB = MAX_PIC_SIZE_IN_MB * 1024 *1024;

	var
		flgError = 0
		,allowedExtsArr = ['jpg','jpeg','gif','png']
		,allowedExtsStr = 'JPG, GIF or PNG'
	;

	if ( profPicMainVal !='' && !isPic_JPG_GIF_PNG(profPicMainVal) )
	{
		msgBox( "The main profile picture must be either " + allowedExtsStr );
		flgError = 1;
	}

	if ( profPicMainVal !='' ) {
		uploadedFileSizeprofPicMain = document.getElementById(profPicMain).files[0].size;
		if ( maxSizeInKB < uploadedFileSizeprofPicMain )
		{
			msgBox( "The main profile picture must be less than " + MAX_PIC_SIZE_IN_MB + " MB." );
			flgError = 1;
		}
	}

	if ( profPic1Val !='' && !isPic_JPG_GIF_PNG(profPic1Val) )
	{
		msgBox( "Pic #1 must be either " + allowedExtsStr );
		flgError = 1;
	}

	if ( profPic1Val !='' ) {
		uploadedFileSizeprofPic1 = document.getElementById(profPic1).files[0].size;
		if ( maxSizeInKB < uploadedFileSizeprofPic1 )
		{
			msgBox( "Pic #1 must be less than " + MAX_PIC_SIZE_IN_MB + " MB." );
			flgError = 1;
		}
	}

	if ( profPic2Val !='' && !isPic_JPG_GIF_PNG(profPic2Val) )
	{
		msgBox( "Pic #2 must be either " + allowedExtsStr );
		flgError = 1;
	}

	if ( profPic2Val !='' ) {
		uploadedFileSizeprofPic2 = document.getElementById(profPic2).files[0].size;
		if ( maxSizeInKB < uploadedFileSizeprofPic2 )
		{
			msgBox( "Pic #2 must be less than " + MAX_PIC_SIZE_IN_MB + " MB." );
			flgError = 1;
		}
	}

	if ( profPic3Val != '' && !isPic_JPG_GIF_PNG(profPic3Val) )
	{
		msgBox( "Pic #3 must be either " + allowedExtsStr );
		flgError = 1;
	}

	if ( profPic3Val !='' ) {
		uploadedFileSizeprofPic3 = document.getElementById(profPic3).files[0].size;
		if ( maxSizeInKB < uploadedFileSizeprofPic3 )
		{
			msgBox( "Pic #3 must be less than " + MAX_PIC_SIZE_IN_MB + " MB." );
			flgError = 1;
		}
	}

	if ( profPic4Val != '' && !isPic_JPG_GIF_PNG(profPic4Val) )
	{
		msgBox( "Pic #4 must be either " + allowedExtsStr );
		flgError = 1;
	}

	if ( profPic4Val !='' ) {
		uploadedFileSizeprofPic4 = document.getElementById(profPic4).files[0].size;
		if ( maxSizeInKB < uploadedFileSizeprofPic4 )
		{
			msgBox( "Pic #4 must be less than " + MAX_PIC_SIZE_IN_MB + " MB." );
			flgError = 1;
		}
	}

	if ( profPicMainVal == '' && profPic1Val == '' && profPic2Val == '' && profPic3Val == '' && profPic4Val == '' )
	{
		msgBox('Nothing to submit!');
		flgError = 1;
	}

	var
		picElementsList = ['profile','profile1','profile2','profile3','profile4']
		picSelectedCount = 0
		;

		for(var i=0; i<picElementsList.length; i++)
		{
		  if( $( "#" + picElementsList[i]).val() != '' )  // Has a value
		  {
		    picSelectedCount++;
		  }
		}

		if( picSelectedCount > 1 )
		{
			flgError = 1;
			msgBox('You can select only one file at a time');
		}

	if ( !flgError )	// If there was no error
	{

		document.getElementById(frmEditProfiePic).submit();
	}
}

function isPic_JPG_GIF_PNG( filename )
{
	var retval = 0;
	var tmpArr = filename.split('.');

	if ( tmpArr.length > 1 )	// If the filename does have an extension
	{
		var ext = tmpArr.pop().toLowerCase();
		if ( ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' )	// Allowed extensions
		{
			retval = 1;
		}
	}
	return retval;
}

function deletePic( picName )
{
	var
		url = 		BASE_URL+'user/ajax_deletePicture'
		,data = 	new Object()
	;

	ans = confirm('Are you sure you want to delete this picture?');
	if ( ans )	// If the user confirmed deletion
	{
		data.txtPicName = 	picName;
		postUsingAjax( url, data, 'afterDeletePic', 'POST' );
	}
}

function afterDeletePic( status, message )
{
	if ( status )	// If deletion was successful
	{
		location.reload();
	}
	else		// Deletion failed
	{
		errMsg = message;
		msgBox(errMsg);
	}
}

function checkNoOfImagesSelected()
{
	var picElementsList =
		['profile','profile1','profile2','profile3','profile4'];

		var picSelectedCount = 0;
		for(var i=0; i<picElementsList.length; i++)
		{
		  if( $( "#" + picElementsList[i]).val() != '' )  // Has a value
		  {
		    picSelectedCount++;
		  }
		}

		if( picSelectedCount > 1 )
		{
		  msgBox('You can select only one file at a time');
		}
}

function clearAllSelectedPics()
{
	var
		picElementsList = ['profile','profile1','profile2','profile3','profile4']
		;
	for(var i=0; i<picElementsList.length; i++)
	{
		$("#" + picElementsList[i]).val('');
	}
}