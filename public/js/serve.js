
function acceptCancelServeRequest(ans, data) {
	if (ans == '1') {
		$.ajax({
			url : BASE_URL + 'serve/commitCancelServe',
			type : 'POST',
			data : {
				sId : data.sId,
				uId : data.uId,
				type: data.type
			},
			dataType : 'json',
			success : function(result) {
				var counter = document.getElementById('totalServer');
				document.getElementById('serverList').style.display = "block";
				if (result.status == '1') {
					counter.innerHTML = parseInt(counter.innerHTML, 10) + 1;
					$('#serveViewMoreSpan').prepend('<span id="viewMore' + data.uId + '" class="span-view-more">' + result.username + '</span>');
					data._this.className = ' btn-g white upper cancelServe';
				} else if (result.status == '3') {
					counter.innerHTML = parseInt(counter.innerHTML, 10) - 1;
					$('#viewMore' + data.uId ).remove();
					data._this.className = ' btn-g white upper acceptServe';
				}

				if (result.status != '0') {
					data._this.innerHTML = result.message;
				}
			},
			error : function(a, b, c) {
				console.log(a);
			}
		});
	}
}


$(document).ready(
	function() {
		$(document).on('click', '.acceptServe', function(e) {
			e.preventDefault();
			if (!isUserLoggedIn() ) {
				yesNoBox('Please Log In');
				return false;
			}
			var ids = this.getAttribute('data-rel').split('-'),
			name = this.getAttribute('data-name'),
			data = {
					sId : ids[0],
					uId : ids[1],
					type : 'accept',
					_this : this
			};
			//yesNoBox('Are you sure you want to serve to ' + name + '?', 'acceptCancelServeRequest', data, true, '500', '140');
			acceptCancelServeRequest(1,data);
		});

		$(document).on('click', '.cancelServe', function(e) {
			e.preventDefault();
			if (!isUserLoggedIn() ) {
				yesNoBox('Please Log In');
				return false;
			}
			var ids = this.getAttribute('data-rel').split('-'),
			name = this.getAttribute('data-name'),
			data = {
				sId : ids[0],
				uId : ids[1],
				type : 'cancel',
				_this : this
			};

			yesNoBox('Are you sure you want to cancel serve to ' + name + '?', 'acceptCancelServeRequest', data, true, '500', '140');
		});

		$('.viewmore').on('click', function(e) {
			e.preventDefault();
			document.getElementById('viewname').value = this.getAttribute('data-rel');
			document.forms['viewpage'].submit();
		});

		$('#serveViewMore').on('click', function(e) {
			e.preventDefault();
			document.getElementById('serveViewMoreSpan').className = "serve-view-more-b";
			this.style.visibility = 'hidden';
		});
});