function ShowAjaxLoading(thisElmId) { 
    $("#"+thisElmId).empty().append('<div style="text-align: center;"><img alt="Loading..." src="/public/icons/indicator-big.gif"></div>');
}
function HideAjaxLoading(thisElmId) {
    $("#" + thisElmId).empty();
}

function JqueryLoadSelectOptionHtml(selValue, targetElm, urlAction)
{
    if(selValue===''){
		$("#" + targetElm).empty().append('<option value="">-- Select --</option>');
		return;
	}
    $.ajax({
        async: false,
        type: "POST",
        url: urlAction +"/"+ selValue,
        contentType: "text/html",
        beforeSend: function () {
            $("#" + targetElm).attr('disabled', 'disabled');
            $("#" + targetElm).addClass('ac_loading');
        },
        
        success: function (htmlResult) {                    
			$("#" + targetElm).empty().append(htmlResult);
			$("#" + targetElm).removeClass('ac_loading');
			$("#" + targetElm).removeAttr('disabled');
     
        }
    });
}

function JqueryLoadCountryStatesHtml(selValue, targetElm, urlAction)
{
       
   if(selValue===''){
		$("#state").empty().append('<option value="">-- Select --</option>');
		return;
	}
    $.ajax({
        async: false,
        type: "POST",
        url: urlAction +"/"+ selValue,
        contentType: "text/html",
        beforeSend: function () {
            $("#state").attr('disabled', 'disabled');
            $("#state").addClass('ac_loading');
        },
        
        success: function (htmlResult) {                    
			$("#" + targetElm).html(htmlResult);
			$("#state").removeClass('ac_loading');
			$("#state").removeAttr('disabled');
     
        }
    });
}


function JqueryAjaxHtml(targetElm, urlAction, showLoading) {    
    $.ajax({
        type: "POST",
        url: urlAction,
        contentType: "text/html",
        beforeSend : (showLoading == "false") ? "" : ShowAjaxLoading(targetElm),
        success: function (htmlResult) {
            $("#" + targetElm).empty().append(htmlResult);
            
        }
    });
}

function jqueryPostForm(thisForm, urlAction) {
    var data = "";
    data = $(thisForm).serialize();
    
    //data = GetJsonFormat(thisForm);
    //data=JSON.stringify(data, "#");
    //data = $.toJSON(data);
    ShowLoadingBox(); // $("#ajaxLoader").show();
    $.ajax({
        async: false,
        type: "POST",
        url: urlAction,
        data: data,
        //data: ({ id: this.getAttribute('id') }),
        //completed: $unblock(),
        success: function (result) {
            //$(this).addClass("done");
            // var domElement = $(result); // create element from html
            // $("#DivFormContent").append(domElement); // append to end of list    
            HideLoadingBox();
            $("#contentFormUpdatePanel").empty().append(result);
            //$.unblockUI();
        }
    });
    HideLoadingBox();
}

function jqueryDelete(action, sno) {
    var data = "";
    $.ajax({
        async: false,
        type: "POST",
        url: action,
        data: data,
        contentType: "text/json",
        success: function (result) {
            // {"ErrNumber":0,"ErrMessage":"","ErrSource":"","ErrType":null,"CheckResponseStatus":true}
            var evlResult = result;//JSON.retrocycle(result);            
            if (evlResult == "true") {
                alert("Record Successfully Deleted");
                DeleteTableListRow(sno);
            } else if (evlResult == "false") {
                alert("Error Deleting Record!\n" + evlResult['ErrMessage'] + "");

            }
        }
    });    
}
function ShowSiblingMessage(elmIdSibling, msg) {
    var objL = $("#" + elmIdSibling).siblings(".error");
    if (objL.length >= 1) {
        $("#" + elmIdSibling).siblings("span.error").html(msg);
    } else {
        $("#" + elmIdSibling).after('<span class="error">' + msg + '</span>');
    }
}


function DeleteTableListRow(thisElm) {
    $("#" + thisElm).css({ "background-color": "red" }).fadeOut(500, function () {
        $("#" + thisElm).remove();
    });
}
function DeleteConfirm(delUrl, delSNo) {

    alertMsg = "Are you sure you want to delete this item?";
    if (confirm(alertMsg)) {
        jqueryDelete(delUrl, delSNo);
    }
    else {
        return false;
    }
}
function getNumber(rnum) { // Arguments: number to round, number of decimal places
    var newnumber = isNaN(parseFloat(rnum)) ? parseFloat(0) : parseFloat(rnum);
    return newnumber;
}

function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
    var newnumber = Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);

    return newnumber;
}
/******************** Scroll to Top **************************************/
(function ($) {
	$.extend($.fn, {
    	scrollWindow : function (options) {
    		var defaults = {            
    				startPosition : 100,
    				speed : 800    			   
    			};
    			
    		var opts = $.extend(defaults, options);    		
    		$(window).scroll(function () {
    			if ($(this).scrollTop() > opts.startPosition) {
    				$('#back-top').fadeIn();
    			} else {
    				$('#back-top').fadeOut();
    			}
    		});
    
    		// scroll body to 0px on click
    		$(this).click(function () {
    			$('body,html').animate({ scrollTop: 0 }, opts.speed);
    			return false;
    		});
    		
    	}
	});
})(jQuery);  

/****************start SetUnset Check box*****************/
/*
how to use 
$(".CheckboxToggle").CheckboxToggle({
		parentChkbox : "#chkBoxParentSelected",
		childChkbox : "chkBoxChildSelected", name[] - array format
		
});*/
(function ($) {
	$.extend($.fn, {
    	checkboxToggle : function (options) {
			var defaults = {            
				parentChkbox : "",
				childChkbox : "",
				isChildChkboxName : true,
				countSelected : 0,
				onclick : $.fn.onChange
			   
			};
			
			var opts = $.extend(defaults, options);
			var countSelected = 0;
			if(opts.parentChkbox == "" || opts.parentChkbox == "")
			{
				alert('Error: No child and parent element found for toggle!!');
			}
			
			
			$(document).on('click', opts.parentChkbox,function() {  
				CheckUncheckAllCheckBoxes(opts.childChkbox, this.checked);  				
			 });  
			
			$(document).on('click',"input[type=checkbox][name*='"+opts.childChkbox+"']",function() {  
				ToggleSelectParentCheckBox(opts.parentChkbox, this.checked,  opts.childChkbox);  				
			}); 	
				
			return $(this).on('click',function()
			{	
				countSelected = $(" input[type=checkbox][name*='"+opts.childChkbox+"']:checked").length;				
				return countSelected;
			});
			
			
									
		},
		onChange : function(opts)
		{	
			var countSelected = $(" input[type=checkbox][name*='"+opts.childChkbox+"'] :checked").size();				
			return countSelected;
		}
	});		
	
})(jQuery);  	

jQuery.fn.ForceNumericOnly = function()
{
	 var key = e.charCode || e.keyCode || 0;
	//var cNumber = ['From-to'];
	var cNumber = ['48-57', '96-105'];
	//var cNumPad = ['96-105'];
	var cAlphabet = ['65-90'];
	var cDecimal = ['110', '190'];
	var cControler = ['8', '9', '13', '35', '36', '37', '38', '39', '40', '46'];
   
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
           
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

$(document).ready(function(){	
	/*
    $("#chkBoxParentSelected").click(function() {  
        CheckUncheckAllCheckBoxes("chkBoxChildSelected", this.checked);  
     });  
    
     $("input[type=checkbox][name*='chkBoxChildSelected']").click(function() {   
        ToggleSelectParentCheckBox("#chkBoxParentSelected", this.checked,  "chkBoxChildSelected");  
    }); */
}); 


// for parent check box--- call function as CheckUncheckAllCheckBoxes(childCheckBoxName, this.checked) {
function CheckUncheckAllCheckBoxes(childCheckBoxName, checkedValue) {	
	var countSelected = 0;
    if (childCheckBoxName == null || childCheckBoxName == undefined )return;    
    $("input[type=checkbox][name*='"+childCheckBoxName+"']").each(function() {
		this.checked = checkedValue;  
    });
	return countSelected;
}
// for child check box--- call function as (parentCheckBoxID, checkedValue, childCheckBoxName) {  
function ToggleSelectParentCheckBox(parentCheckBoxID, checkedValue, childCheckBoxName) {  
	 //alert(parentCheckBoxID +", "+ checkedValue+", "+ childCheckBoxName);
     if (parentCheckBoxID == null || parentCheckBoxID == undefined)  
         return;  
     if (!checkedValue || checkedValue == false)
     {   
          $(parentCheckBoxID).attr("checked", false);  
     }
	 else
	 {  	
        var areAllChecked = true;  
        $("input[type=checkbox][name*='"+childCheckBoxName+"']").each(function() {      
					     	
             if (!this.checked) {  			 	
                 areAllChecked = false;
				 return;
             }  
         });
        $(parentCheckBoxID).prop('checked',areAllChecked);
         //$(parentCheckBoxID).attr("checked", areAllChecked);  
     }  
}  



/****************start SetUnset Check box*****************/
	
/*$(function () {
    $(document).ajaxError(function (event, request, options) {
        //if (request.status === 403) {//$("#access-denied-dialog").dialog({width: 500,height: 400,modal: true,buttons: {Ok: function () {$(this).dialog("close");}}});}else 
		alert(request.status)
        if (request.status === 401) {
            ShowThickBox('Session Expired', '#TB_inline?height=300&width=400&modal=true&inlineId=divPageSessionExpiredInfo');
        }
		if (jqXHR.status === 0) {
			data = ('Not connect.\n Verify Network.');
		} else if (jqXHR.status == 404) {
			data = ('Requested page not found. [404]');
		} else if (jqXHR.status == 500) {
			data = ('Internal Server Error [500].');
		} else if (exception === 'parsererror') {
			data = ('Requested JSON parse failed.');
		} else if (exception === 'timeout') {
			data = ('Time out error.');
		} else if (exception === 'abort') {
			data = ('Ajax request aborted.');
		} else {
			data = ('Uncaught Error.\n' + jqXHR.responseText);
		}
    });


});*/