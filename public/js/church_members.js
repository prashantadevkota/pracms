function confirmDelete(memberId)
{
	var msg= "Are you sure, you want to remove this member from the Church?";
	yesNoBox(msg, 'removeMember', memberId );
}

function removeMember( ans, memberId )
{
	var url = BASE_URL + 'church/removemember/' + memberId;
	if ( ans == 1 )	// If the user clicked the yes button
	{
		ajaxCallUrl( url, 'afterRemoveMember' );
	}
}

function afterRemoveMember(status, message)
{
	if ( status )	// If the news was successfully deleted
	{
		location.reload();
	}
	else	// There was an error while deleting the news record
	{
		alert( message );
	}
}
