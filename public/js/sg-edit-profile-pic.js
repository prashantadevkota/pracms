var
	profPics = 		[
	           		 'pic1'
	           		 ,'pic2'
	           		 ,'pic3'
	           		 ,'pic4'
	           		 ,'pic5'
	           		 ,'pic6'
	           		 ,'pic7'
	           		 ,'pic8'
	           		]
	,numPics = 				profPics.length
	,frmEditProfiePic = 	'frmEditOtherProfilePic'
;

function verifyPictures()
{
	var maxSizeInKB = 	MAX_PIC_SIZE_IN_MB * 1024 *1024;
	var
		flgError = 0
		,somethingToSubmit = 0
		,selectedCount = 0
		,allowedExtsArr = ['jpg','jpeg','gif','png']
		,allowedExtsStr = 'JPG, GIF or PNG'
	;

	for( var i=0; i<numPics; i++ )
	{
		var pic =  	document.getElementById( profPics[i] );
		
		if ( pic.value !='' ) 		// If there is something to check
		{
			selectedCount++;
			if ( selectedCount >1 )	// If more than one picture was selected
			{
				msgBox('You can select only one file at a time');
				flgError = 1;
				break;
			}
			
			if ( !isPic_JPG_GIF_PNG(pic.value) )	// If not an allowed type
			{
				msgBox( "Pic #" + (i+1) + " must be either " + allowedExtsStr );
				flgError = 1;
				break;
			}
			
			var uploadedFileSizeprofPic = pic.files[0].size;
			if ( maxSizeInKB < uploadedFileSizeprofPic )	// Pic exceeds allowed MAX_SIZE
			{
				msgBox( "Pic #" + (i+1) + " must be less than " + MAX_PIC_SIZE_IN_MB + " MB." );
				flgError = 1;
				break;
			}
		}
	}

	if( !flgError )		// If there were no errors
	{
		if( selectedCount == 0 )	// If nothing is selected
		{
			msgBox('Nothing to submit!');
			flgError = 1;
		}
		else	// If the user did everything correctly, then submit the form
		{
			document.getElementById(frmEditProfiePic).submit();
			flgError = 1;
		}
	}
}

function isPic_JPG_GIF_PNG( filename )
{
	var retval = 0;
	var tmpArr = filename.split('.');

	if ( tmpArr.length > 1 )	// If the filename does have an extension
	{
		var ext = tmpArr.pop().toLowerCase();
		if ( ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' )	// Allowed extensions
		{
			retval = 1;
		}
	}
	return retval;
}

function deletePic( picName, sgId )
{
	yesNoBox('Are you sure you want to delete this picture?', 'deletePicAns', 
				{'picName':picName, 'sgId': sgId} 
			);
}

function deletePicAns( ans, args )
{
	if( ans == 1 )	// If the user clicked on Yes
	{
		var
			url = 			BASE_URL+'smallgroup/deletePicture'
			,data = 		new Object()
		;
		data.txtPicName = 	args.picName;
		data.txtSgId =		args.sgId;
		postUsingAjax( url, data, 'afterDeletePic', 'POST' );
	}
}

function afterDeletePic( status, message )
{
	if ( status )	// If deletion was successful
	{
		location.reload();
	}
	else		// Deletion failed
	{
		errMsg = message;
		msgBox(errMsg);
	}
}

function clearAllSelectedPics()
{
	for( var i=0; i<numPics; i++ )
	{
		$("#" + profPics[i]).val('');
	}
}