
function requestOrCancelJoin(ans, data) {
	if (ans == '1') {
		$.ajax({
			url : BASE_URL + 'smallgroup/requestorcanceljoin',
			type : 'POST',
			data : {
				sgId : data.sgId,
				uId : data.uId,
				type: data.type
			},
			dataType : 'json',
			success : function(result) {
				data._this.innerHTML = result.message;
				if (result.status == '2') {
					data._this.className = ' btn-g border-none white font14 cancelJoin';
				} else if (result.status == '1') {
					data._this.className = ' btn-g border-none white font14 requestJoin';
				}

			},
			error : function(a, b, c) {
				console.log(data);
				console.log(a);
			}
		});
	}
}


$(document).ready(
	function() {
		$(document).on('click', '.requestJoin', function(e) {

			e.preventDefault();
			if (!isUserLoggedIn() ) {
				yesNoBox('Please Log In');
				return false;
			}
			var ids = this.getAttribute('data-rel').split('-'),
			name = this.getAttribute('data-name'),
			data = {
					sgId : ids[1],
					uId : ids[0],
					type : 'request',
					_this : this
			};
			yesNoBox('Are you sure you want to join ' + name + ' group?', 'requestOrCancelJoin', data, true, '500', '140');
//			requestOrCancelJoin(1,data);
		});

		$(document).on('click', '.cancelJoin', function(e) {
			e.preventDefault();
			if (!isUserLoggedIn() ) {
				yesNoBox('Please Log In');
				return false;
			}
			var ids = this.getAttribute('data-rel').split('-'),
			name = this.getAttribute('data-name'),
			data = {
				sgId : ids[1],
				uId : ids[0],
				type : 'cancel',
				_this : this
			};

			yesNoBox('Are you sure you want to cancel your join-request to ' + name + ' group?', 'requestOrCancelJoin', data, true, '500', '140');
		});

//		$('.viewmore').on('click', function(e) {
//			e.preventDefault();
//			document.getElementById('viewname').value = this.getAttribute('data-rel');
//			document.forms['viewpage'].submit();
//		});
//
//		$('#serveViewMore').on('click', function(e) {
//			e.preventDefault();
//			document.getElementById('serveViewMoreSpan').className = "serve-view-more-b";
//			this.style.visibility = 'hidden';
//		});
});