function underConstruction()
{
	msgBox('Under Construction');
}

/*
 * Needed javascript functions in this file
*/
function ajaxCallUrl( url, callback_function )
{
	  var method = 'GET', data= new Object();
	  $.ajax( {
	    url:        url,
	    type:       method,
	    data:       data,
	    dataType:   'json',
	    success:    function( result ) {
	      if ( result != null )
	      {
	        if( ! result.hasOwnProperty('status') )    // Remember to pass the status, else it will be set to 0
	          result.status = 0;
	        if( ! result.hasOwnProperty('message') )
	            result.message = '';
	          if( ! result.hasOwnProperty('data') )
	              result.data = '';

	        if ( typeof callback_function == 'function' )   // If a function itself was passed
	        {
	          callback_function.call( this, result.status, result.message, result.data );  // 'this' is necessary but doesn't count as an argument (strange)
	        }
	        else if ( typeof window[callback_function] == 'function' )    // If just the name of the function was passed
	        {
	          if( ! result.hasOwnProperty('message') )
	            result.message = '';
	          window[callback_function]( result.status, result.message, result.data );
	        }

	        return result.status;
	      }
	      else
	      {
	        return 0;
	      }
	    },
	    error:      function ( result ) {
	      if( ! result.hasOwnProperty('message') )    // Remember to pass the status, else it will be set to 0
	        result.status = 0;

	      if ( result != null )
	      {
	        return result.status;
	      }
	      else
	        return 0;
	    }
	  } );
}

function postUsingAjax( url, data, callback_function, method )
{
  var method = ( typeof method === 'undefined' ? 'POST' : method );
  $.ajax( {
	async: 		false,
    url:        url,
    type:       method,
    data:       data,
    dataType:   'json',
    success:    function( result ) {
      if ( result != null )
      {
        if( ! result.hasOwnProperty('status') )    // Remember to pass the status, else it will be set to 0
          result.status = 0;
        if( ! result.hasOwnProperty('message') )
            result.message = '';
          if( ! result.hasOwnProperty('data') )
              result.data = '';

        if ( typeof callback_function == 'function' )   // If a function itself was passed
        {
          callback_function.call( this, result.status, result.message, result.data );  // 'this' is necessary but doesn't count as an argument (strange)
        }
        else if ( typeof window[callback_function] == 'function' )    // If just the name of the function was passed
        {
          if( ! result.hasOwnProperty('message') )
            result.message = '';
          window[callback_function]( result.status, result.message, result.data );
        }

        return result.status;
      }
      else
      {
        return 0;
      }
    },
    error:      function ( result ) {
      if( ! result.hasOwnProperty('message') )    // Remember to pass the status, else it will be set to 0
        result.status = 0;

      if ( result != null )
      {
        return result.status;
      }
      else
        return 0;
    }
  } );
}

function submitFormUsingAjax( formId, callbackFuncAfterFormSubmit, funcToCallJustBeforeFormSubmit, params )
{
  var   form = document.getElementById(formId)
      , submitUrl = form.action
      , data = new Object()
      , subUError = 'No URL given to submit form to!'
      ;

  // Check if there is a value for the submitUrl for the form
  if ( submitUrl == '' )
  {
    if ( typeof params === 'object' && params != null )    // Params has to an associative array & must contain a key named 'submit_url'
    {
      if ( 'submit_url' in params )
      {
        submitUrl = params['submit_url'];
      }
      else
      {
        alert( subUError );
        return false;
      }
    }
    else    // If params is scalar or null or doesn't contain a key named 'submit_url'
    {
      alert( subUError );
      return false;
    }
  }

  // Now extract data from the form
  $('#' + formId + ' *').filter(':input').each(
    function(index, element)
    {
      data[element.name] = $(element).val();
    }
  );
  // END_OF data-extracting from the form loop

  var res = true;
  // If we have a function to call just before submitting the form, and it exists
  if ( funcToCallJustBeforeFormSubmit != null && typeof window[funcToCallJustBeforeFormSubmit] === 'function' )
  {
    // If you want to cancel form submission, return false from your function
    res = window[funcToCallJustBeforeFormSubmit]();		// Dynamically Call the function
  }
  if ( res )
  {
    res = postUsingAjax( submitUrl, data, callbackFuncAfterFormSubmit, 'POST' );
  }
  return res;
}

//Will return 0 on No, 1 on Yes, -1 on Close (Cancel)
function yesNoBox( msg, callback_function, args, modal, width, height )
{
	var
		self = this.yesNoBox
		,thisFuncName = 'yesNoBox'
		,yesBtnClickedFuncStr = 'parent.yesNoBox.yesBtnClicked'
		,noBtnClickedFuncStr = 'parent.yesNoBox.noBtnClicked'
		,cancelBtnClickedFuncStr = 'parent.yesNoBox.cancelBtnClicked'
		,yesVal=1, noVal=0, cancelVal=-1
		;
	msg = 					typeof msg == 'undefined' ? 'There should be a question here. Did you forget?' : msg;
	modal = 				typeof modal == 'undefined' ? false : modal ? true : false;
	width = 				typeof width == 'undefined' ? 300 : width;
	height = 				typeof height == 'undefined' ? 150 : height;
	args =					typeof args == 'undefined' ? '' : args;

	self.callback_function = typeof callback_function == 'function' ? callback_function :
								typeof window[callback_function] == 'function' ? window[callback_function] : '';
	self.clickedBtn = '';
	self.yesBtnClicked = function(){
							$.colorbox.close();
							this.clickedBtn = 'yes';
							if ( this.callback_function != '' )
							{
								this.callback_function( yesVal, args );
							}
							//alert('You clicked Yes');
						};
	self.noBtnClicked = function() {
							$.colorbox.close();
							this.clickedBtn = 'no';
							if ( this.callback_function != '' )
							{
								this.callback_function( noVal, args );
							}
							//alert('You clicked No');
						};
	self.cancelBtnClicked = function() {
							if ( this.callback_function != '' && this.clickedBtn == '' )
							{
								this.callback_function( cancelVal, args );
							}
						};

	var html = 	"<div class='ynb_msg' id='ynb_msg1'>" + makeTagsSafe(msg) + "</div>"
				+ 	"<div class='ynb_btncontainer'> \
						<input type='button' value='Yes' onClick='" + yesBtnClickedFuncStr + "();' class='ynb_btn'> \
						<input type='button' value='No' onClick='" + noBtnClickedFuncStr + "();' class='ynb_btn'> \
					</div>"
					;
	$.colorbox({html:html, iframe:false, onClosed: function() { parent.yesNoBox.cancelBtnClicked(); }, overlayClose: modal, width: width, height: height });
}

/** Will return 0 on No, 1 on Yes, -1 on Close (Cancel)
 * If you want to call some other function from inside the page loaded by Colorbox,
 * use: parent.msgBox.callback_function(args...). But make sure you pass the callback_function as an argument
 * when calling msgBox.
 * @param msg
 * @param callback_function
 * @param args
 * @param modal
 * @param width
 * @param height
 */
function msgBox( msg, callback_function, args, modal, width, height )
{
	var
		self = this.msgBox
		,thisFuncName = 'msgBox'
		,okBtnClickedFuncStr = 'parent.' + thisFuncName + '.okBtnClicked'
		,cancelVal=-1
		;
	msg = 					typeof msg == 'undefined' ? 'There should be a message here. Did you forget?' : msg;
	modal = 				typeof modal == 'undefined' ? false : modal ? true : false;
	width = 				typeof width == 'undefined' ? 300 : width;
	height = 				typeof height == 'undefined' ? 150 : height;
	args =					typeof args == 'undefined' ? '' : args;

	self.callback_function = typeof callback_function == 'function' ? callback_function :
								typeof window[callback_function] == 'function' ? window[callback_function] : '';
	self.clickedBtn = '';
	self.okBtnClicked = function(){
							$.colorbox.close();
							this.clickedBtn = 'ok';
							if ( this.callback_function != '' )
							{
								this.callback_function( 1, args );
							}
						};
	self.cancelBtnClicked = function() {
							if ( this.callback_function != '' && this.clickedBtn == '' )
							{
								this.callback_function( cancelVal, args );
							}
						};
	var html = 		"<div class='mb_msg'>" + makeTagsSafe(msg) + "</div>"
				+ 	"<div class='mb_btncontainer'> \
						<input type='button' value='Ok' onClick='" + okBtnClickedFuncStr + "();' class='mb_btn'> \
					</div>"
					;
	if( isValidUrl(msg) )	// Meant to display a page
	{
		$.colorbox({href:msg, iframe:false, onClosed: function() { parent.msgBox.cancelBtnClicked(); }, overlayClose: modal, width: width, height: height });
	}
	else	// Simply meant to display a message
	{
		$.colorbox({html:html, iframe:false, onClosed: function() { parent.msgBox.cancelBtnClicked(); }, overlayClose: modal, width: width, height: height });
	}
}

function loadUrl(url, width, height, modal, args)
{
	modal = 				typeof modal == 'undefined' ? false : modal ? true : false;
	width = 				typeof width == 'undefined' ? 600 : width;
	height = 				typeof height == 'undefined' ? 500 : height;
	args =					typeof args == 'undefined' ? '' : args;
	$.colorbox({href:url, iframe:false, onClosed: function() { return 1; }, overlayClose: false, width: width, height: height });
}

/**
 * Fastest way to make HTML tags safe in JS
 * Ref: http://stackoverflow.com/questions/5499078/fastest-method-to-escape-html-tags-as-html-entities
 * @param str
 * @returns str with HTML tags made safe against js injection
 */
function makeTagsSafe(str)
{
    return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
}

/**
 * Returns true if the str is a valid URL, else returns false.
 * Ref: http://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-an-url/14582229#14582229
 * @param str
 * @returns {Boolean}
 */
function isValidUrl(str)
{
	var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
	  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
	  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
	  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
	  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
	  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
	if(!pattern.test(str))
	{
		return false;
	}
	else
	{
		return true;
	}
}

//************* Function Prototypes for not supported functions in various Browsers *******************
// Trim isn't supported in IE8
if(typeof String.prototype.trim !== 'function')
{
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}
//************* END_OF Function Prototypes for not supported functions in various Browsers ************

/* To submit forms by pressing ENTER */
$(document).ready(
function()
{
	$("input").keypress(function(event) 
	{
	    if (event.which == 13) 
	    {
	        event.preventDefault();
	        
	        if( document.getElementById('frmLogin') )
	        {
	        	tryLogin();
	        }
	        else
	        {
	        	$("#btnSubmit").click();
//	        	document.forms[0].submit();
	        }
	    }
	});
});
/* END_OF To submit forms by pressing ENTER */

function tryLogin()
{
	submitFormUsingAjax( 'frmLogin', 'tryLogin_callback' );
}

function tryLogin_callback( status, message )
{
	if( status ) // If the login was successful
	{
		window.location = '/admin';
	}
	else	// Login was unsuccessful
	{
		msgBox( message );
	}
}

function logout()
{
	ajaxCallUrl( '/user/ajax_logout', 'logout_callback' );
}

function logout_callback( status, message )
{
	if( status ) // If the login was successful
	{
		window.location = '/';
	}
	else	// Login was unsuccessful
	{
		msgBox( message );
	}
}

function createProduct()
{
	submitFormUsingAjax( 'frmCreateProduct', 'createProduct_callback' );
}

function createProduct_callback( status, message, data )
{
	msgBox( message );
	
	if( status )	// If product was added successfully
	{
		clearProductForm();
	}
}

function clearProductForm()
{
	$("#txtTitle").val('');
	$("#filImage").val('');
	$("#txtPrice").val('');
	$("#txaDescription").val('');
}

function disable( prodId )
{
	postUsingAjax( '/admin/disableproduct/' + prodId, null, disable_callback );
}

function disable_callback( status, message, data )
{
//	msgBox( message, null, null, true );
	window.location.reload();
}


function enable( prodId )
{
	postUsingAjax( '/admin/enableproduct/' + prodId, null, enable_callback );
}

function enable_callback( status, message, data )
{
//	msgBox( message, null, null, true );
	window.location.reload();
}
