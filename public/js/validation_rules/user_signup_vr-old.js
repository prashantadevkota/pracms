$().ready( function() 
{
	$("#frmUserSignup").validate({
		rules: {
			txtFirstName: {
				required: true,
				minlength: 3
			},
			txtLastName: {
				required: true,
				minlength: 3
			},
			txtUserName: {
				required: true,
				minlength: 3
			},
			txtPassword: {
				required: true,
				minlength: 6
			},
			txtPassword2: {
				required: true,
				minlength: 6,
				equalTo: "#txtPassword"
			},
			txtCity:{
				required: true
			},
			selState:{
				//selectcheckState: true,
				required: true
			},
			txtEmail: {
				required: true,
				email: true
			},
			selDay: {
				//selectcheckDay: true,
				required: true
			},
			selMonth: {
				//selectcheckMonth: true,
				required: true
			},
			selYear: {
				//selectcheckYear: true,
				required: true
			}
		},
		messages: {
			txtFirstName: {
				required: "Please provide your firstname",
				minlength: "Your firstname must be at least 3 characters long"
			},
			txtCity: {
				required: "Please enter the name of your City",
			},
			selState: {
				required: "Please select your state"
			},
			selDay: {
				required: "Please select the day"
			},
			selMonth: {
				required: "Please select the month"
			},
			selYear: {
				required: "Please select the year"
			},
			txtLastName: {
				required: "Please provide your lastname",
				minlength: "Your lastname must be at least 3 characters long"
			},
			txtUserName: {
				required: "Please provide your username",
				minlength: "Your username must be at least 3 characters long"
			},
			txtPassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			},
			txtPassword2: {
				required: "Please confirm your password",
				minlength: "Your confirmed password must be at least 6 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address"
		}
	});
/*	
	jQuery.validator.addMethod('selectcheckDay', function (value, element) {
        return (value != '0');
    }, "Select a Day");
	
	jQuery.validator.addMethod('selectcheckMonth', function (value, element) {
        return (value != '0');
    }, "Select a Month");
	
	jQuery.validator.addMethod('selectcheckYear', function (value, element) {
        return (value != '0');
    }, "Select a Year");
*/
});