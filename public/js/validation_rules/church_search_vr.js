$().ready( function() 
{
	$("#frmChurchSignUp").validate({
		rules: {
			
			txtChurchName: {
				required: true
			},
			
			txtChurchCity: {
				required: true
			},
			
			selChurchState: {
				required: true
			}
		},
		messages: {
			
			txtChurchName: {
				required: "Please enter the name of your Church."
			},
			
			txtChurchCity: {
				required: "Please select your city."
			},
			
			selChurchState: {
				required: "Please select your state."
			}
		}
	});
});