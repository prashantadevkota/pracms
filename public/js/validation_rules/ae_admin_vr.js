$().ready( function() 
{
	$("#frmCreateSmallGroup").validate({
		rules: {
			
			txtName: {
				required: true
			},
			
			selCampus: {
				required: true
			},
			
			selGroupLeader: {
				required: true
			},
			
			txtDay:{
				required:true
			},
			
			txtFrequency:{
				required:true
			},
			
			txtLocation:{
				required:true
			},
			
			txtTopic:{
				required:true
			},
			
			txtAgePreference:{
				required:true
			},
			
			txtTimeOfDay:{
				required:true
			}
			
		},
		messages: {
			
			txtName: {
				required: "Please enter the name of your Small Group."
			},
			
			selCampus: {
				required: "Please select your Campus."
			},
			
			selGroupLeader: {
				required: "Please select name of your Group leader."
			},
			
			txtDay:{
				required:"please enter day."
			},
			
			txtFrequency:{
				required:"please enter your frequency."
			},
			
			txtLocation:{
				required:"Please enter your location."
			},
			
			txtTopic:{
				required:"please enter your topic."
			},
			
			txtAgePreference:{
				required:"Please enter your Age Preferance."
			},
			
			txtTimeOfDay:{
				required:"Please enter time of day."
			}
			
		}
	});
});
