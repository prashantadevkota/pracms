$().ready(function() {
	$("#editVerse").validate({
		rules : {
			txaVerse : {
				required : true
			},
			txtVerseAct : {
				required : true

			}
		},
		messages : {
			txaVerse : {
				required : "Please include a verse.",

			},
			txtVerseAct : {
				required : "Please enter the verse act. "
			}
		}
	});

});