$().ready( function() 
{
	$("#frmEditUserProfile").validate({
		ignore: [],
		
		errorPlacement: function(error, element) {
		    if (element.attr("name") == "selHomeChurch" )
		        error.insertAfter(".selectize-control");
		},
		
		rules: {
			txtFirstName: {
				required: true,
				minlength: 3
			},
			txtLastName: {
				required: true,
				minlength: 3
			},
			txtCity:{
				required: true
			},
			selState:{
				required: true
			},
			txtEmail: {
				required: true,
				email: true
			},
			selDay: {
				required: true
			},
			selMonth: {
				required: true
			},
			selYear: {
				required: true
			},
			txtUrl: {
				checkUrl: true
			},
			txtZip: {
				required: true
			},
			selHomeChurch: {
				required: true
			}
		},
		messages: {
			txtFirstName: {
				required: "Please provide your firstname",
				minlength: "Your firstname must be at least 3 characters long"
			},
			txtCity: {
				required: "Please enter the name of your City",
			},
			selState: {
				required: "Please select your state"
			},
			selDay: {
				required: "Please select the day"
			},
			selMonth: {
				required: "Please select the month"
			},
			selYear: {
				required: "Please select the year"
			},
			txtLastName: {
				required: "Please provide your lastname",
				minlength: "Your lastname must be at least 3 characters long"
			},
			txtZip: {
				required: "Plese enter you ZIP code."
			},
			selHomeChurch: {
				required: "Please select your Home Church."
			},
			email: "Please enter a valid email address"
		}
	});
	jQuery.validator.addMethod('checkUrl', function (value, element) {
		var retval = false;
		if ( value == 'http://' || value == '' )
		{
			retval = true;
		}
		else
		{
			retval = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(value);
		}
		return retval;
    }, "Please enter a valid URL, please do it!");
});