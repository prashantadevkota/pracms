$().ready( function() 
{
	$("#frmUserSignup").validate({
		rules: {
			txtFirstName: {
				required: true,
				minlength: 3
			},
			txtLastName: {
				required: true,
				minlength: 3
			},
			txtEmail: {
				required: true,
				email: true
			},
			txtPassword_S: {
				required: true,
				minlength: 6
			},
			txtPassword_S2: {
				required: true,
				minlength: 6,
				equalTo: "#txtPassword_S"
			}
		},
		messages: {
			txtFirstName: {
				required: "Please provide your firstname",
				minlength: "Your firstname must be at least 3 characters long"
			},
			txtLastName: {
				required: "Please provide your lastname",
				minlength: "Your lastname must be at least 3 characters long"
			},
			txtEmail: {
				required: "Please enter a valid email address",
				email: "Please enter a valid email address"
			},
			txtPassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			},
			txtPassword2: {
				required: "Please confirm your password",
				minlength: "Your confirmed password must be at least 6 characters long",
				equalTo: "Please enter the same password as above"
			}
		}
	});
});