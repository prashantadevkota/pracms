$().ready(function() {
	$("#frmChangePwd").validate({
		rules : {
			txtOldPwd : {
				required : true,
				minlength: 6
			},
			txtNewPwd : {
				required : true,
				minlength: 6
			},
			txtNewPwd2 : {
				required : true,
				equalTo: "#txtNewPwd"
			}
		},
		messages : {
			txtOldPwd : {
				required : "Please provide your current password",
				minlength: "Your current password should be at least 6 chars long"
			},
			txtNewPwd : {
				required : "Please provide a new password",
				minlength: "Your new password should be at least 6 chars long"
			},
			txtNewPwd2 : {
				required : "Please confirm your new password",
				equalTo: 	"Passwords do not match"
			}
		}
	});
});