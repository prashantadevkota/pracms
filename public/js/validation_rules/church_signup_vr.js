$().ready( function() 
{
	$("#churchSignUpForm").validate({
		rules: {
			txtAdminName: {
				required: true,
				minlength: 3
			},
			
			txtAdminEmail: {
				required: true,
				email: true
			},
			
			txtAdminPhone: {
				required: true
			},
			
			txtAdminPassword: {
				required: true,
				minlength: 6
			},
			
			txaChurchStory: {
				required: true,
				minlength: 6
			},
			
			txtChurchName: {
				required: true
			},
			
			txtChurchPhone: {
				required: true
			},
			
			txtAdminPassword2: {
				required: true,
				minlength: 6,
				equalTo: "#txtAdminPassword"
			},
			
			txtChurchCity: {
				required: true
			},
			
			txaChurchMission: {
				required: true,
				minlength: 6
			},
			
			selChurchState: {
				required: true
			},
			
			txtAdminPassword2: {
				required: true,
				minlength: 6,
				equalTo: "#txtAdminPassword"
			},
			
			txtChurchEmail: {
				email: true
			},
			
			chkConfirmAdmin: {
				required: true
			}
		},
		messages: {
			txtAdminName: {
				required: "Please provide your Admin Name.",
				minlength: "Your firstname must be at least 3 characters long."
			},
			txtAdminEmail: {
				required: "Please enter your email address.",
				email: "Please enter a valid email address."
			},
			txtAdminPhone: {
				required: "Please enter your phone number."
			},
			
			txaChurchStory: {
				required: "Please write down your Church story."
			},
			
			txtChurchName: {
				required: "Please enter the name of your Church."
			},
			
			txtChurchPhone: {
				required: "Please enter your Church phone number."
			},
			
			txaChurchMission: {
				required: "Please write down your Church mission."
			},
			
			txtChurchCity: {
				required: "Please select your city."
			},
			
			selChurchState: {
				required: "Please select your state.",
			},
			
			txtAdminPassword: {
				required: "Please provide a password.",
				minlength: "Your password must be at least 6 characters long."
			},
			
			txtAdminPassword2: {
				required: "Please confirm your password",
				minlength: "Your confirmed password must be at least 6 characters long.",
				equalTo: "Please enter the same password as above."
			},
			
			txtChurchEmail: {
				email: "Please enter a valid email address for your Church."
			},
			
			chkConfirmAdmin: {
				required: "Please certify that you are this Church's authorized Admin."
			}
		}
	});
});