$().ready( function() 
{
	$("#frmCreateSmallGroup").validate({
		ignore: [],
		rules: {
			txtName: {
				required: true
			},
			
			selCampus: {
				required: true
			},
			
			selGroupLeader: {
				required: true
			},
			
			txtDay : {
				required: true
			},
			
			txtTimeOfDay: {
				required: true
			},
			
			txtFrequency: {
				required: true
			},
			
			txtLocation: {
				required: true
			},
			
			txtTopic: {
				required: true
			},
			
			txtAgePreference: {
				required: true
			},
			
			rdoChildCare: {
				required: true
			}
			
		},
		messages: {
			txtName: {
				required: "Please write down your Small group's name ."
			},
			
			selCampus: {
				required: "Please select your Campus."
			},
			
			selGroupLeader: {
				required: "Please select your group Leader."
			},
			
			txtDay : {
				required: "Please mention the day."
			},
			
			txtTimeOfDay: {
				required: "Please mention the time of day."
			},
			
			txtFrequency: {
				required: "Please mention the frequency."
			},
			
			txtLocation: {
				required: "Please mention the location."
			},
			
			txtTopic: {
				required: "Please mention the topic."
			},
			
			txtAgePreference: {
				required: "Please mention age preference."
			},
			
			rdoChildCare: {
				required: "Select child care availability."
			}
		}
	});
});