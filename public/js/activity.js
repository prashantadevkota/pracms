/**
 * Works for the following actions: 'like', 'unlike', 'pray', 'unpray'
 */
var
wallPostForm = 		'wallPostForm'	,
statusUpdateForm=	'statusUpdateForm'// This is form for wall post
;
function doAction( action, acted_ent_type, acted_ent_id, owner_ent_type, owner_ent_id, item1, item2 )
{
	var
		action = 				(typeof(action) == 'undefined' ? '' : action).toLowerCase()
		,acted_ent_type = 		(typeof(acted_ent_type) == 'undefined' ? 0 : acted_ent_type)
		,acted_ent_id = 		(typeof(acted_ent_id) == 'undefined' ? 0 : acted_ent_id)
		,owner_ent_type = 		(typeof(owner_ent_type) == 'undefined' ? 0 : owner_ent_type)
		,owner_ent_id = 		(typeof(owner_ent_id) == 'undefined' ? 0 : owner_ent_id)
	;
	if ( action == '' || acted_ent_type == 0 || acted_ent_id == 0
		 //|| parent_acted_ent_type == 0 || parent_acted_ent_id == 0
		)
	{
		alert('Please do not try and manipulate the system');
		return;
	}
	else	// If the passed args are OK
	{
		if( !flgUserLoggedIn && !flgChurchAdminLoggedIn )	// If user isn't logged in
		{
			msgBox('Please login first');
			return;
		}
		else if( flgChurchAdminLoggedIn )	// If a Church Admin is logged in
		{
			msgBox('You cannot perform this action');
			return;
		}

		var
			url = 		BASE_URL+'activity/' + action
			,data = 	new Object()
		;

		data.acted_ent_type = 			acted_ent_type;
		data.acted_ent_id = 			acted_ent_id;
		data.owner_ent_type = 			owner_ent_type;
		data.owner_ent_id = 			owner_ent_id;
		data.item1 = 					item1;
		data.item2 = 					item2;

		//postUsingAjax( url, data, 'after_' + action, 'POST' );
		postUsingAjax( url, data, 'after_' + action, 'POST' );
	}
}

function after_like( status, message, data )
{
	if( status == 1 )	// If the action was successful
	{
		var item1Display='none', item2Display='block';
		$('#' + data.item1).css( 'display', item1Display );
		$('#' + data.item2).css( 'display', item2Display );

		// Now do something about the like list
		var
			activityId = data.activityId
			,listContainer = 'likeList_' + activityId + '_Data'
			,val1 = $('#' + listContainer).html()
			;
		// If we already pulled like list, then we need to add this like to the list
		if( val1 != 'Loading...' )
		{
			$('#' + listContainer).html( 'You<br>' + val1);
		}
		// END_OF Now do something about the like list
	}
	else if ( status == 2 )		// If the user has already liked the item
	{
		alert( 'You have already liked this item' );
	}
	else
	{
//		alert( 'Unknown error!' );
		alert( message );
	}
}

function after_unlike( status, message, data )
{
	if( status == 1 )	// If the action was successful
	{
		var item1Display='block', item2Display='none';
		$('#' + data.item1).css( 'display', item1Display );
		$('#' + data.item2).css( 'display', item2Display );

		// Now do something about the like list
		var
			activityId = data.activityId
			,listContainer = 'likeList_' + activityId + '_Data'
			,val1 = $('#' + listContainer).html().replace( 'You<br>', '')
			;
		// If we already pulled like list, then we need to remove this like from the list
		if( val1 != 'Loading...' )
		{
			$('#' + listContainer).html( val1 );
		}
		// END_OF Now do something about the like list
	}
	else if ( status == 2 )		// If the user has already unliked the item
	{
		alert( 'You have already unliked this item' );
	}
	else
	{
//		alert( 'Unknown error!' );
		alert( message );
	}
}

function after_pray( status, message, data )
{
	if( status == 1 )	// If the action was successful
	{
		var item1Display='none', item2Display='block';
		$('#' + data.item1).css( 'display', item1Display );
		$('#' + data.item2).css( 'display', item2Display );

		// Now do something about the pray list
		var
			activityId = data.activityId
			,listContainer = 'prayList_' + activityId + '_Data'
			,val1 = $('#' + listContainer).html()
			;
		// If we already pulled pray list, then we need to add this prayer to the list
		if( val1 != 'Loading...' )
		{
			$('#' + listContainer).html( 'You<br>' + val1);
		}
		// END_OF Now do something about the like list
	}
	else if ( status == 2 )		// If the user has already prayed for the item
	{
		alert( 'You have already prayed for this item' );
	}
	else
	{
		//alert( 'Unknown error!' );
		alert( message );
	}
}

function after_unpray( status, message, data )
{
	if( status == 1 )	// If the action was successful
	{
		var item1Display='block', item2Display='none';
		$('#' + data.item1).css( 'display', item1Display );
		$('#' + data.item2).css( 'display', item2Display );

		// Now do something about the pray list
		var
			activityId = data.activityId
			,listContainer = 'prayList_' + activityId + '_Data'
			,val1 = $('#' + listContainer).html().replace( 'You<br>', '')
			;
		// If we already pulled pray list, then we need to remove this prayer from the list
		if( val1 != 'Loading...' )
		{
			$('#' + listContainer).html( val1 );
		}
		// END_OF Now do something about the like list
	}
	else if ( status == 2 )		// If the user has already unprayed for the item
	{
		alert( 'You have already removed your prayer for this item' );
	}
	else
	{
		//alert( 'Unknown error!' );
		alert( message );
	}
}

/**
 * Works for the following actions: 'like', 'unlike', 'pray', 'unpray'
 */
function afterDoAction( status, message, data )
{
	if( status == 1 )	// If the action was successful
	{
		var item1Display='none', item2Display='block';
		if( data.toggle == 1 )
		{
			item1Display='block';
			item2Display='none';
		}
		$('#' + data.item1).css( 'display', item1Display );
		$('#' + data.item2).css( 'display', item2Display );
	}
	else if ( status == 2 )		// If the user has already liked the item
	{
		alert( 'You have already ' + data.action + 'ed this item' );
	}
	else
	{
		alert( 'Unknown error!' );
	}
}

function commentOnActivity( acted_ent_type, acted_ent_id, owner_ent_type, owner_ent_id )
{

	var
		 acted_ent_id = 			(typeof(acted_ent_id) == 'undefined' ? 0 : acted_ent_id)
		,owner_ent_type = 			(typeof(owner_ent_type) == 'undefined' ? 0 : owner_ent_type)
		,owner_ent_id = 			(typeof(owner_ent_id) == 'undefined' ? 0 : owner_ent_id)
		;

	if ( acted_ent_id == 0 )
	{
		alert('Please do not try and manipulate the system');
		return;
	}
	else	// If the passed args are OK
	{
		if( !flgUserLoggedIn && !flgChurchAdminLoggedIn )	// If user isn't logged in
		{
			msgBox('Please login first');
			return;
		}
		else if( flgChurchAdminLoggedIn )	// If a Church Admin is logged in
		{
			msgBox('You cannot perform this action');
			return;
		}

		var
			url = 			BASE_URL+'activity/comment'
			,data = 		new Object()
			,comments = 	$('#txaActivityComment_' + acted_ent_id).val();
		;
		var res = validateCommentByActivityId ( acted_ent_id );
		if( res )
		{
			data.comments = 			comments;
			data.acted_ent_type = 		acted_ent_type;
			data.acted_ent_id = 		acted_ent_id;
			data.owner_ent_type = 		owner_ent_type;
			data.owner_ent_id = 		owner_ent_id;
			postUsingAjax( url, data, 'afterCommentOnActivity', 'POST' );
		}
	}
}

function afterCommentOnActivity( status, message, data )
{
	if ( !status )	// If the update wasn't successful
	{
		alert(message);
	}
	else	// If the update was successful
	{
		var
			activityId = 		data.activityId
			,actedEntId = 	data.acted_ent_id
			//,beforeNode =	document.getElementById('hidAppendLatestCommentAfterMe_'+actedEntId)
			//,beforeNode =	document.getElementById('txaActivityComment_' + actedEntId )
			,beforeNode =	document.getElementById('hidPrependUserCommentsBeforeMe_' + actedEntId )
			;
		var
			parent = 		document.getElementById('containerComment_' + actedEntId)
			//parent = 		document.getElementById('commentForm_' + actedEntId)
			,child = 		document.createElement('div')
			;
		var
			ccount = parseInt( $('#comment_'+actedEntId).text().trim().substr(8) ) + 1
		;
		$('#comment_'+actedEntId).text('Comment(' + ccount + ')');
		$('#txaActivityComment_'+actedEntId).val('');

		child.innerHTML = 	data.feed;
		//parent.insertBefore(child, beforeNode.nextSibling);
		parent.insertBefore(child, beforeNode);
		setReadMoreAbridgedHeight();

		//alert('Successfully updated your status');
	}
}


function showMoreComments( acted_ent_type, acted_ent_id, uid_for_actions )
{
	var
	url = 		BASE_URL+'activity/getmorecommentlist'
	,data = 	new Object()
	,lLimit = 	0
	,numRec = 	3
	,uLimit = 	lLimit+numRec
	;

	data.activity_id =		acted_ent_id;
	data.uidForActions =	uid_for_actions;
	data.lowerLimit =		$("#hidLL_commentList_" + acted_ent_id).val();
	data.recNum =			$("#hidLRC_commentList_" + acted_ent_id).val() ;

	postUsingAjax( url, data, 'afterShowMoreComments', 'POST' );
}

function afterShowMoreComments ( status, message, data )
{
	if( status )	// If the request was successful
	{
		var
			feedDataSvr = 	data.feed
			,feedNumItems = feedDataSvr.length
			,activityId = 	data.activityId
			,loadingDiv = 	document.getElementById('divCommentLoading_' + activityId) 
			,container = 	document.getElementById('containerComment_' + activityId )
			,showMoreCommentsSpan=document.getElementById('showMoreComments_' + activityId )
			,recNum =		parseInt($("#hidLRC_commentList_" + activityId).val())
			;

		if ( feedNumItems > 0 )		// Only if the feed has something
		{
			var
				child = 		document.createElement('div')
				,parent = 		document.getElementById('containerComment_' + activityId)
				//,parent = 		document.getElementById('commentForm_' + activityId)
				//,beforeNode =	document.getElementById('txaActivityComment_' + activityId )
				,beforeNode =	document.getElementById('hidPrependLatestCommentsBeforeMe_' + activityId )
				,feedData = 	''
				,i
				;

			for( i=0; i<feedNumItems; i++ )
			{
				feedData += feedDataSvr[i];
			}
			child.innerHTML = 	feedData;
			parent.insertBefore(child, beforeNode);
			$("#hidLL_commentList_" + activityId).val( parseInt(recNum + data.lowerLimit) );
			setReadMoreAbridgedHeight();

			if( feedNumItems < recNum )	// We can hide showMore because this is the last set of comments
			{
				showMoreCommentsSpan.style.display=  	'none';
			}
		}
		else //if there are no  feed items yet
		{
			showMoreCommentsSpan.style.display=  	'none';
		}
		container.style.display = 	'block';
		loadingDiv.style.display=  	'none';
	}
	else	// An error occurred during the request
	{
		alert('Sorry an error occurred. Please try again after some time');
	}
}

function showComments( acted_ent_type, acted_ent_id, uid_for_actions )
{
	var
		divName = 'commentList_' + acted_ent_id
		,tmpDiv = document.getElementById(divName)
		;
	tmpDiv.style.display = 'block';

	// Now get the initial list of comments from the server
	showMoreComments( acted_ent_type, acted_ent_id, uid_for_actions );
}

function showLikeList( activityId )
{
	var
		url = 		BASE_URL+'activity/getlikelist'
		,data = 	new Object()
		,listContainer = 'likeList_' + activityId + '_Data'
		,val1 = $('#' + listContainer).html()
		;
	if( val1 == 'Loading...')	// If we haven't pulled the like list, pull it!
	{
		data.activity_id = activityId;
		postUsingAjax( url, data, 'afterShowLikeList', 'POST' );
	}
	else	// If we have already pulled the like list
	{
		if ( val1 != '' )	// If we have something to display
		{
			var divName = 'likeList_' + activityId;
			$('#' + divName).css('display','block');
		}
	}
}

function hideLikeList( activityId )
{
	var divName = 'likeList_' + activityId;
	$('#' + divName).css('display','none');
}

function afterShowLikeList( status, message, data )
{
	if( status )
	{
		var likeList = data.likeList, likeListLen=likeList.length;
		var divName = 'likeList_' + data.activity_id,
			dataDivName = 'likeList_' + data.activity_id + '_Data',
			nameList = '',
			tmpName = '';
		if( likeListLen > 0 )	// If people have liked this
		{
//console.log(likeList);
			for(var i=0; i<likeListLen; i++)
			{
				tmpName = likeList[i].name;
				if( tmpName == myName )
					tmpName = 'You';
				nameList += tmpName + '<br>';
			}
			$('#' + dataDivName).html(nameList);
			$('#' + divName).css('display','block');
		}
		else	// Even if no people have liked this, set it to blank
		{
			$('#' + dataDivName).html(nameList);
		}
	}
}

function showPrayList( activityId )
{
	var
		url = 		BASE_URL+'activity/getpraylist'
		,data = 	new Object()
		,listContainer = 'prayList_' + activityId + '_Data'
		,val1 = $('#' + listContainer).html()
		;
	if( val1 == 'Loading...')	// If we haven't pulled the prayed list, pull it!
	{
		data.activity_id = activityId;
		postUsingAjax( url, data, 'afterShowPrayList', 'POST' );
	}
	else	// If we have already pulled the pray list
	{
		if ( val1 != '' )	// If we have something to display
		{
			var divName = 'prayList_' + activityId;
			$('#' + divName).css('display','block');
		}
	}
}

function hidePrayList( activityId )
{
	var divName = 'prayList_' + activityId;
	$('#' + divName).css('display','none');
}

function afterShowPrayList( status, message, data )
{
	if( status )
	{
//console.log(data);
		var prayList = data.prayList, prayListLen=prayList.length,
			divName = 'prayList_' + data.activity_id,
			dataDivName = 'prayList_' + data.activity_id + '_Data',
			nameList = '',
			tmpName = '';
		if( prayListLen > 0 )	// If people have prayed for this
		{
			var divName = 'prayList_' + data.activity_id,
				dataDivName = 'prayList_' + data.activity_id + '_Data',
				nameList = '';
//console.log(likeList);
			for(var i=0; i<prayListLen; i++)
			{
				tmpName = prayList[i].name;
				if( tmpName == myName )
					tmpName = 'You';
				nameList += tmpName + '<br>';
			}

			$('#' + dataDivName).html(nameList);
			$('#' + divName).css('display','block');
		}
		else	// Even if no people have prayed for this, set it to blank
		{
			$('#' + dataDivName).html(nameList);
		}
	}
}

function showServeCommittedList( activityId, serveId )
{
	var
		url = 		BASE_URL+'activity/getservecommitedlist'
		,data = 	new Object()
		,listContainer = 'serveCommList_' + activityId + '_Data'
		,val1 = $('#' + listContainer).html()
		;
	if( val1 == 'Loading...')	// If we haven't pulled the prayed list, pull it!
	{
		data.activity_id = activityId;
		data.serve_id = serveId;
		postUsingAjax( url, data, 'afterShowServeCommittedList', 'POST' );
	}
	else	// If we have already pulled the pray list
	{
		if ( val1 != '' )	// If we have something to display
		{
			var divName = 'serveCommList_' + activityId;
			$('#' + divName).css('display','block');
		}
	}
}

function hideServeCommittedList ( activityId )
{
	var divName = 'serveCommList_' + activityId;
	$('#' + divName).css('display','none');
}

function afterShowServeCommittedList ( status, message, data )
{
	if( status )
	{
//console.log(data);
		var serveCommList = data.serveCommitedList, listLen=serveCommList.length,
			divName = 'serveCommList_' + data.activity_id,
			dataDivName = 'serveCommList_' + data.activity_id + '_Data',
			nameList = '',
			tmpName = '';
		if( listLen > 0 )	// If people have committed to this SERVE_EVENT
		{
			var divName = 'serveCommList_' + data.activity_id,
				dataDivName = 'serveCommList_' + data.activity_id + '_Data',
				nameList = '';
//console.log(likeList);
			for(var i=0; i<listLen; i++)
			{
				tmpName = serveCommList[i].name;
				if( tmpName == myName )
					tmpName = 'You';
				nameList += tmpName + '<br>';
			}

			$('#' + dataDivName).html(nameList);
			$('#' + divName).css('display','block');
		}
		else	// Even if no people have committed to this SERVE_EVENT, set it to blank
		{
			$('#' + dataDivName).html(nameList);
		}
	}
}

function postToWall( acted_ent_type, acted_ent_id, parent_acted_ent_type, parent_acted_ent_id
		, owner_ent_type, owner_ent_id, posttowallofuser )
{
	var
		acted_ent_type = 			(typeof(acted_ent_type) == 'undefined' ? 0 : acted_ent_type)
		,acted_ent_id = 			(typeof(acted_ent_id) == 'undefined' ? 0 : acted_ent_id)
		,parent_acted_ent_type = 	(typeof(parent_acted_ent_type) == 'undefined' ? 0 : parent_acted_ent_type)
		,parent_acted_ent_id = 		(typeof(parent_acted_ent_id) == 'undefined' ? 0 : parent_acted_ent_id)
		,owner_ent_type = 			(typeof(owner_ent_type) == 'undefined' ? 0 : owner_ent_type)
		,owner_ent_id = 			(typeof(owner_ent_id) == 'undefined' ? 0 : owner_ent_id)
		,comments = 				$('#comments').val();
		;

	// This line is to fix the IE placeholder bug (has to be executed before a form submit)
	if ( comments == $('#comments').attr('placeholder') )	// Remove text if same as placeholder
	{
		$('#comments').val('');
		comments = '';
	}

	if ( acted_ent_type == 0 || acted_ent_id == 0 || parent_acted_ent_type == 0 || parent_acted_ent_id == 0 )
	{
		alert('Please do not try and manipulate the system');
		return;
	}
	else	// If the passed args are OK
	{
		var
			action = posttowallofuser ? 'posttowallofuser' : 'posttowall'
			,url = 		BASE_URL+'activity/' + action
			,data = 	new Object()
		;

		var res = $("#"+wallPostForm).valid(); //validation first
		if (res)
		{
			data.comments = 				comments;
			data.acted_ent_type = 			acted_ent_type;
			data.acted_ent_id = 			acted_ent_id;
			data.parent_acted_ent_type = 	parent_acted_ent_type;
			data.parent_acted_ent_id = 		parent_acted_ent_id;
			data.profile_name = 			thisProfileName;
			data.owner_ent_type = 			owner_ent_type;
			data.owner_ent_id = 			owner_ent_id;
			postUsingAjax( url, data, 'afterPostToWall', 'POST' );
		}
	}
}

function afterPostToWall( status, message, data )
{
	if ( !status )	// If the update wasn't successful
	{
		alert(message);
	}
	else	// If the update was successful
	{
		var beforeNode =	document.getElementById('hidAppendWallPostsAfterMe');
		var
			 parent = 		document.getElementById('divActivityFeed')
			,child = 		document.createElement('div')
			;
		child.innerHTML = 	data;
		parent.insertBefore(child, beforeNode.nextSibling);
		setReadMoreAbridgedHeight();
		$('#comments').val('');
	}
}

function updateStatus()
{
	var
		url = 		BASE_URL+'user/ajax_updatestatus'
		,data = 	new Object()
		,statusUpdate = $("#txaStatusUpdate").val()
	;
	// This line is to fix the IE placeholder bug (has to be executed before a form submit).
	if ( statusUpdate == $('#txaStatusUpdate').attr('placeholder') )	// Remove text if same as placeholder
	{
		$('#txaStatusUpdate').val('');
		statusUpdate = '';
	}

	var res = $("#"+statusUpdateForm).valid(); //validation first
	if (res)
	{
		data.txaStatusUpdate = 		statusUpdate;
		data.chkPrayerReq = 		document.getElementById("chkPrayerReq").checked ? 1 : 0;
		postUsingAjax( url, data, 'afterUpdateStatus', 'POST' );
	}
}

function afterUpdateStatus( status, message, data )
{
	if ( !status )	// If the update wasn't successful
	{
		alert(message);
	}
	else	// If the update was successful
	{
		var beforeNode =	document.getElementById('h1Activity');
		var
			 parent = 		document.getElementById('divActivityFeed')
			,child = 		document.createElement('div')
			,feedData = 	''
			;
		child.innerHTML = 	data;
		parent.insertBefore(child, beforeNode.nextSibling);
		setReadMoreAbridgedHeight();

		// Make the status update textfield blank
		$("#txaStatusUpdate").val('');

		//$('.readmore').readmore({abridged_height: '4em'});	// For readmore
	}
}

function showOlderPosts(flgProfilePage)
{
	var
		url = 		BASE_URL+'activity/getMoreActivityList'
		,url2 = 	BASE_URL+'activity/getMoreActivityListForProfilePage'
		,data = 	new Object()
		,lLimit = 	0
		,numRec = 	3
		,uLimit = 	lLimit+numRec
		,flgProfilePage = (typeof(flgProfilePage) == 'undefined' ? 0 : flgProfilePage)
		;

	data.profileUid = 	parseInt( $("#hidProfileUid").val() );
	data.lowerLimit = 	parseInt( $("#hidLowerLimit").val() );
	data.recNum = 		parseInt( $("#hidLimitRecCount").val() );
	data.lowerLimit += 	data.recNum;	// Because when page loads we already load 'recNum' amount of posts

	url = 				flgProfilePage == '1' ? url2 : url;
	postUsingAjax( url, data, 'afterShowOlderPosts', 'POST' );
}

function afterShowOlderPosts( status, message, data )
{
	var beforeNode =	document.getElementById('divShowOlderPost');

	if( status )	// If the request was successful
	{
		var feedNumItems = data.feed.length;
		if ( feedNumItems > 0 )		// Only if the feed has something
		{
			var
				 parent = 		document.getElementById('divActivityFeed')
				,child = 		document.createElement('div')
				,feedData = 	''
				,i
				;
			for( i=0; i<feedNumItems; i++ )
			{
				feedData += data.feed[i];
			}
			child.innerHTML = 	feedData;
			parent.insertBefore(child, beforeNode);
			$("#hidLowerLimit").val(data.lowerLimit);

			setReadMoreAbridgedHeight();
			//$('.readmore').readmore({abridged_height: '4em'});	// For readmore
		}
		else	// If the feed has nothing (is empty)
		{
			beforeNode.style.display = 'none';
		}
	}
	else	// An error occurred during the request
	{
		alert('Sorry an error occurred. Please try again after some time');
	}
}

function showOlderPostsForChurch()
{
	var
	url = 		BASE_URL+'activity/getMoreActivityListForChurchProfile'
	,data = 	new Object()
	,lLimit = 	0
	,numRec = 	3
	,uLimit = 	lLimit+numRec
	;

data.churchId = 	parseInt( $("#hidChurchId").val() );
data.lowerLimit = 	parseInt( $("#hidLowerLimit").val() );
data.recNum = 		parseInt( $("#hidLimitRecCount").val() );
data.lowerLimit += 	data.recNum;	// Because when page loads we already load 'recNum' amount of posts

postUsingAjax( url, data, 'afterShowOlderPosts', 'POST' );
}

function showOlderPostsForSmallGroup()
{
	var
	url = 		BASE_URL+'activity/getMoreActivityListForSmallGroupProfile'
	,data = 	new Object()
	,lLimit = 	0
	,numRec = 	3
	,uLimit = 	lLimit+numRec
	;

	data.sgId = 		parseInt( $("#hidSgId").val() );
	data.lowerLimit = 	parseInt( $("#hidLowerLimit").val() );
	data.recNum = 		parseInt( $("#hidLimitRecCount").val() );
	data.lowerLimit += 	data.recNum;	// Because when page loads we already load 'recNum' amount of posts

	postUsingAjax( url, data, 'afterShowOlderPosts', 'POST' );
}


function acceptRejectConnection(ans, data)
{
	var m = data._this.getAttribute('data-name').split('-'),
		msg = m['1'].replace(' with', '').replace(' from', '');
	if (ans == '1') {
		$.ajax({
			url : BASE_URL + 'user/acceptRejectRequest',
			type : 'POST',
			data : {
				uId : data.uId,
				mId : data.mId,
				type : data.type
			},
			dataType : 'json',
			success : function(result) {
				rel = data._this.getAttribute('data-rel').split('-');
				if (rel.length == 2) {
					//notification page
					if (document.getElementById('spn' + data.mId) != null) {
						document.getElementById('spn' + data.mId).innerHTML = '';
					}
					if ( data.type == 'accept' ) {
						document.getElementById('span' + data.mId).innerHTML = ' is now a connection.';
					} else if ( data.type == 'reject' ) {
						document.getElementById('span' + data.mId).innerHTML = ' - ' + msg + ' cancelled.';
					}
				} else {
					//profile page
					var count= data._this.getAttribute('data-rel').split('-').length,
					extra_class= '';
					if(count==4){
						extra_class= 'font10';
					}
					var id = data._this.getAttribute('id');
					data._this.innerHTML = 'Connect';
					data._this.className = (id == null) ? 'btn-g border-none white fl' +extra_class+  ' requestConn' :'connection requestConn';
				}
			},
			error : function(a) {
				console.log(a);
			}
		});
	}
}

function sendRequest(ans, data)
{
	if (ans == '1') {
		$.ajax({
			url : BASE_URL + 'user/sendRequest',
			type : 'POST',
			data : {
				uId : data.uId,
				mId : data.mId
			},
			dataType : 'json',
			success : function(result) {
				if (result.status == '1') {
			var count= data._this.getAttribute('data-rel').split('-').length,
			extra_class= '';
			if(count==4){
				extra_class= 'font10';
			}

//console.log(data._this.getAttribute('data-rel').split('-'));
//console.log(count);

					var id = data._this.getAttribute('id');
					data._this.innerHTML = result.message;
//					rel = data._this.getAttribute('data-rel').split('-');
//					rel[0] = result.connId;
//					data._this.setAttribute('data-rel', rel.join('-'));
					data._this.className = (id == null) ? 'btn-g border-none white fl ' +extra_class+ ' rejectConn' :'connection rejectConn';
				}
			},
			error : function(a, b, c) {
				console.log(a);
			}
		});
	}
}


$(document).ready(
	function() {
		$(document).on('click', '.acceptConn', function(e) {
			e.preventDefault();
			if (!isUserLoggedIn() ) {
				msgBox('Please Log In');
				return false;
			}
			var relData = this.getAttribute('data-name').split('-'),
			ids = this.getAttribute('data-rel').split('-'),
			data = {
					uId : ids[0],
					mId : ids[1],
					type : 'accept',
					_this : this
			};
			yesNoBox('Are you sure you want to connect with ' + relData[0] + '?', 'acceptRejectConnection', data, true, '500', '140');
		});

		$(document).on('click', '.rejectConn', function(e) {
			e.preventDefault();
			if (!isUserLoggedIn() ) {
				msgBox('Please Log In');
				return false;
			}
			var relData = this.getAttribute('data-name').split('-'),
			ids = this.getAttribute('data-rel').split('-'),
				data = {
					uId : ids[0],
					mId : ids[1],
					type : 'reject',
					_this : this
			};
			yesNoBox('Are you sure you want to cancel ' + relData[1] + ' ' + relData[0] + '?', 'acceptRejectConnection', data, true, '500', '140');
		});

		$(document).on('click', '.requestConn', function(e){
			e.preventDefault();
			if (!isUserLoggedIn() ) {
				msgBox('Please Log In');
				return false;
			}
			var ids = this.getAttribute('data-rel'),
				relData = this.getAttribute('data-name').split('-'),
				data = {
					uId : ids.split('-')[0],
					mId : ids.split('-')[1],
					_this : this
			};
			yesNoBox('Are you sure you want to connect with ' + relData[0] + '?', 'sendRequest', data, true, '500', '140');
		});
});

function reportText(acted_ent_type, acted_ent_id, parent_acted_ent_type, parent_acted_ent_id, data)
{
	var url= BASE_URL+'/popups/reportinappropriate/' +
				acted_ent_type + '/' + acted_ent_id + '/'
				+ parent_acted_ent_type + '/' + parent_acted_ent_id
		,w=600
		,h=300
		,data = (typeof(data) == 'undefined' ? (new Object()) : data)
		;
	msgBox(url, 'afterReport', '', false, w, h, data );
}

function hideReport(acted_ent_id)
{
	var
	divName = 'containerReport_' + acted_ent_id
	,tmpDiv = document.getElementById(divName)
	;
	tmpDiv.style.display = 'none';
}

function handleReportInappActions( status, message, data )
{
	if ( status == 1 )	// If the report was successful
	{
		msgBox('Your report has been recorded!');
	}
	else if( status == -1 )		// The user cancelled
	{
		// Do nothing
	}
	else	// If the report wasn't successful
	{
		msgBox( message );
	}
}

function ReportedInappropriate( )
{
	msgBox('You have already reported this.');
}

function validateCommentByActivityId( activityId )
{
	var
		rules = 		{}
		,messages = 	{}
	;
	rules["txaActivityComment_"+ activityId] = 		{ required: true };
	messages["txaActivityComment_"+ activityId] = 	{ required: "Please write down something" };

	$("#commentForm_" + activityId).validate({
		rules : rules
		,messages : messages
	});
	return $("#commentForm_" + activityId).valid();
}

function approveSmallGroupMembers(sgId, userId)
{

	var
		url = 		BASE_URL+'smallgroup/approvemembers'
		,data = 	new Object()
	;
		data.sgId = 		sgId;
		data.userId = 		userId;
		postUsingAjax( url, data, 'afterApproveSmallGroupMembers','POST' );
}

function afterApproveSmallGroupMembers(status, message, data)
{	var userId=data.userId
		,hideButtons = 	document.getElementById('divAcceptRejectButtons_'+userId)
		,showStatus =	document.getElementById('divAcceptRejectStatus_'+userId);
	
	hideButtons.style.display=  	'none';
	showStatus.style.display=		'block';
}

function deleteSmallGroupMembers(sgId, userId)
{
	var
		url = 		BASE_URL+'smallgroup/deletemember'
		,data = 	new Object()
	;
		data.sgId = 		sgId;
		data.userId = 		userId;
		postUsingAjax( url, data, 'afterDeleteSmallGroupMembers','POST' );
}

function afterDeleteSmallGroupMembers()
{
	location.reload(); 
}



