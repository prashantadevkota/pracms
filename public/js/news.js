function confirmDelete(newsId)
{
	var msg= "Are you sure, you want to delete this news?";
	yesNoBox(msg, 'deleteNews', newsId );
}

function deleteNews( ans, newsId )
{
	var url = BASE_URL + 'news/delete/' + newsId;
	if ( ans == 1 )	// If the user clicked the yes button
	{
		ajaxCallUrl( url, 'afterDeleteNews' );
	}
}

function afterDeleteNews(status, message)
{
	if ( status )	// If the news was successfully deleted
	{
		var newsId = message.id;
		$("#tr_" + newsId).remove();
	}
	else	// There was an error while deleting the news record
	{
		alert( message );
	}
}
