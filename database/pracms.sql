-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 21, 2014 at 10:25 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pracms`
--
CREATE DATABASE IF NOT EXISTS `pracms` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `pracms`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `prod_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` float NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `create_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`prod_id`, `title`, `price`, `description`, `create_time`, `status`) VALUES
(1, 'Test1', 39, 'klllk', '2014-08-21 19:35:47', 0),
(4, 'Test2', 99, 'Test2', '2014-08-21 19:46:26', 0),
(5, 'Test3', 195, 'Test3', '2014-08-21 19:46:40', 0),
(6, 'Prashanta', 999, 'This is good', '2014-08-21 20:17:53', 0),
(7, 'Apple', 45, 'An apple a day...', '2014-08-21 20:30:39', 1),
(8, 'Zendy', 34, 'Zendy Mendy', '2014-08-21 20:31:06', 0),
(9, 'Samsung Galaxy''s 4', 666, 'Samsung mobile phone edited version', '2014-08-21 21:21:25', 0),
(10, 'Nokia''s super duper gooper', 23, 'Nokia Lumia NOT in same price', '2014-08-21 21:34:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `email_hash` varchar(64) COLLATE utf8_bin NOT NULL,
  `password` varchar(64) COLLATE utf8_bin NOT NULL,
  `salt` varchar(64) COLLATE utf8_bin NOT NULL,
  `activation_hash` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `forgotpwd_hash` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `role_id` tinyint(4) NOT NULL DEFAULT '2',
  `status` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `email`, `email_hash`, `password`, `salt`, `activation_hash`, `forgotpwd_hash`, `create_time`, `role_id`, `status`) VALUES
(1, 'admin@pracms.com', '', '27631f9d2b3767e9efdc1c1c8a5c01a7', 'prashanta', NULL, NULL, '2014-08-22 00:00:00', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
